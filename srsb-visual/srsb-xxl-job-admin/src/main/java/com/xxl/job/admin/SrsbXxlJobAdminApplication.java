package com.xxl.job.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author 朱耀威
 */
@EnableDiscoveryClient
@SpringBootApplication
public class SrsbXxlJobAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(SrsbXxlJobAdminApplication.class, args);
	}

}