/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the srsb4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.zyw.srsb.bulletin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyw.srsb.bulletin.dto.db.EducationDto;
import com.zyw.srsb.bulletin.dto.db.YearNumDto;
import com.zyw.srsb.bulletin.dto.response.DataListResponse;
import com.zyw.srsb.bulletin.dto.response.PartyBranchStatisResponse;
import com.zyw.srsb.bulletin.dto.response.SexRatioResponse;
import com.zyw.srsb.bulletin.dto.response.TableResponse;
import com.zyw.srsb.bulletin.entity.SrsbBulletinPartyMember;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 党员信息表
 *
 * @author 朱耀威
 * @date 2022-01-23 15:58:17
 */
@Mapper
public interface SrsbBulletinPartyMemberMapper extends BaseMapper<SrsbBulletinPartyMember> {
	/**
	 * 统计各个党支部人数
	 * @return
	 */
	List<PartyBranchStatisResponse> queryPartyBranchStatistics();

	/**
	 * 查询党员各个年龄信息
	 * @return
	 */
	List<TableResponse> queryAnnualPopulationStatistics();

	/**
	 * 查询学历男女数量信息
	 */
	List<EducationDto> queryPartyEducationalStatistics();

	/**
	 * 查询对应的学历名称 根据sort排序
	 */
	List<String> queryEducationList();

	/**
	 * 查询男女比例
	 */
	SexRatioResponse querySexRatio();

	/**
	 * 年份统计
	 */
	List<YearNumDto> queryPartyMemberDevelopmentResponse();

}
