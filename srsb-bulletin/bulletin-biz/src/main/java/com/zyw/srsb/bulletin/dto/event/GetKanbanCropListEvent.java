package com.zyw.srsb.bulletin.dto.event;

import lombok.Data;

@Data
public class GetKanbanCropListEvent {
	private String name;
	private String value;
}
