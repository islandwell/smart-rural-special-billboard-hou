package com.zyw.srsb.bulletin.dto.db;

import lombok.Data;

import java.math.BigInteger;

@Data
public class TaxInfoDto {
	private BigInteger sale;
	private BigInteger taxAmount;
}
