package com.zyw.srsb.bulletin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyw.srsb.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 智慧农村词云表
 *
 * @author 朱耀威
 * @date 2022-05-07 11:58:54
 */
@Data
@TableName("srsb_bulletin_wordcloud_info")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "智慧农村词云表")
public class SrsbBulletinWordcloudInfo extends BaseEntity {

    /**
     * 主键id
     */
    @TableId
    @ApiModelProperty(value="主键id")
    private Integer id;

    /**
     * 名称
     */
    @ApiModelProperty(value="名称")
    private String name;

    /**
     * 值
     */
    @ApiModelProperty(value="值")
    private Integer value;


}
