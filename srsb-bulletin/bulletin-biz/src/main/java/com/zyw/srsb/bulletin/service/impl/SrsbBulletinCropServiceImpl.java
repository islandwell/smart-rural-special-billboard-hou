package com.zyw.srsb.bulletin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyw.srsb.bulletin.dto.event.GetKanbanCropListEvent;
import com.zyw.srsb.bulletin.entity.SrsbBulletinCrop;
import com.zyw.srsb.bulletin.mapper.SrsbBulletinCropMapper;
import com.zyw.srsb.bulletin.service.SrsbBulletinCropService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 农作物
 *
 * @author 朱耀威
 * @date 2022-05-04 16:59:30
 */
@Service
public class SrsbBulletinCropServiceImpl extends ServiceImpl<SrsbBulletinCropMapper, SrsbBulletinCrop> implements SrsbBulletinCropService {

	@Override
	public List<GetKanbanCropListEvent> getMerchantOperation() {
		return this.baseMapper.getMerchantOperation();
	}
}
