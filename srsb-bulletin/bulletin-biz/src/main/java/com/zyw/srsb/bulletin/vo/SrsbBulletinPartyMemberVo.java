package com.zyw.srsb.bulletin.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
/**
 * 党员信息
 * @author 朱耀威
 * @date 2022/3/19
 */
@Data
@ColumnWidth(30)
public class SrsbBulletinPartyMemberVo implements Serializable {
	private static final long serialVersionUID = 3791280844838288057L;

	/**
	 * 所属党组织
	 */
	@ApiModelProperty(value="所属党组织")
	@ExcelProperty("所属党组织")
	private String partyOrganization;

	/**
	 * 党组织code
	 */
	@ApiModelProperty(value="党组织code")
	@ExcelProperty("党组织Code")
	private String code;

	/**
	 * 姓名
	 */
	@ApiModelProperty(value="姓名")
	@ExcelProperty("姓名")
	private String fullName;

	/**
	 * 身份证号码
	 */
	@ApiModelProperty(value="身份证号码")
	@ExcelProperty("身份证号码")
	@NotBlank(message = "身份证号码")
	private String cardNo;

	/**
	 * 手机号码
	 */
	@ApiModelProperty(value="手机号码")
	@ExcelProperty("手机号码")
	private String phoneNo;

	/**
	 * 交纳党费
	 */
	@ApiModelProperty(value="交纳党费")
	@ExcelProperty("交纳党费")
	private Long payPartyDues;

	/**
	 * 性别 0-男 1-女
	 */
	@ApiModelProperty(value="性别 0-男 1-女")
	@ExcelProperty("性别")
	private String gender;

	/**
	 * 年龄
	 */
	@ApiModelProperty(value="年龄")
	@ExcelProperty("年龄")
	private Integer age;

	/**
	 * 党龄
	 */
	@ApiModelProperty(value="党龄")
	@ExcelProperty("党龄")
	private Integer partyAge;

	/**
	 * 学历
	 */
	@ApiModelProperty(value="学历")
	@ExcelProperty("学历")
	private String education;

	/**
	 * 参加工作时间
	 */
	@ApiModelProperty(value="参加工作时间")
	@ExcelProperty("参加工作时间")
	private String workingTime;

	/**
	 * 名族
	 */
	@ApiModelProperty(value="民族")
	@ExcelProperty("民族")
	private String nation;

	/**
	 * 出生年月
	 */
	@ApiModelProperty(value="出生年月")
	@ExcelProperty("出生年月")
	private String birthday;

	/**
	 * 入党年月
	 */
	@ApiModelProperty(value="入党年月")
	@ExcelProperty("入党年月")
	private String joinDate;

	/**
	 * 专业
	 */
	@ApiModelProperty(value="专业")
	@ExcelProperty("专业")
	private String major;

	/**
	 * 任职单位
	 */
	@ApiModelProperty(value="任职单位")
	@ExcelProperty("任职单位")
	private String employer;

	/**
	 * 最后交费时间
	 */
	@ApiModelProperty(value="最后交费时间")
	@ExcelProperty("最后交费年月")
	private String lastPaymentDate;

}
