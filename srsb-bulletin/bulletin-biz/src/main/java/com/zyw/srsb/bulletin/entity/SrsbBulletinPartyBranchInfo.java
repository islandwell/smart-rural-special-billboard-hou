package com.zyw.srsb.bulletin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyw.srsb.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 党支部管理
 *
 * @author 朱耀威
 * @date 2022-04-09 20:28:21
 */
@Data
@TableName("srsb_bulletin_party_branch_info")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "党支部管理")
public class SrsbBulletinPartyBranchInfo extends BaseEntity {

    /**
     * id
     */
    @TableId
    @ApiModelProperty(value="id")
    private Integer id;

    /**
     * 名称
     */
    @ApiModelProperty(value="名称")
    private String name;

    /**
     * 地址
     */
    @ApiModelProperty(value="地址")
    private String address;

    /**
     * 经度
     */
    @ApiModelProperty(value="经度")
    private String longitude;

    /**
     * 纬度
     */
    @ApiModelProperty(value="纬度")
    private String latitude;

    /**
     * 坐标系
     */
    @ApiModelProperty(value="坐标系")
    private String pointSystem;

    /**
     * 党支部介绍
     */
    @ApiModelProperty(value="党支部介绍")
    private String context;

    /**
     * 成立时间
     */
    @ApiModelProperty(value="成立时间")
    private LocalDateTime establishmentTime;

    /**
     * 状态
     */
    @ApiModelProperty(value="状态")
    private Integer status;


}
