package com.zyw.srsb.bulletin.dto.event;

import lombok.Data;

import java.math.BigInteger;

@Data
public class GetBaseInfoEvent {
	public GetBaseInfoEvent() {
		this.area = BigInteger.ZERO;
		this.num = BigInteger.ZERO;
		this.typeNum = BigInteger.ZERO;
		this.sale = BigInteger.ZERO;
	}

	/**
	 * 种植面积
	 */
	private BigInteger area;
	/**
	 * 农户数量
	 */
	private BigInteger num;
	/**
	 * 作物种类
	 */
	private BigInteger typeNum;
	/**
	 * 作物销量
	 */
	private BigInteger sale;
}
