package com.zyw.srsb.bulletin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyw.srsb.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 村庄信息表
 *
 * @author 朱耀威
 * @date 2022-03-27 14:51:01
 */
@Data
@TableName("srsb_bulletin_village_information")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "村庄信息表")
public class SrsbBulletinVillageInformation extends BaseEntity {

    /**
     * 主键id
     */
    @TableId
    @ApiModelProperty(value="主键id")
    private Long id;

    /**
     * 村庄名称
     */
    @ApiModelProperty(value="村庄名称")
    private String name;

    /**
     * 经度
     */
    @ApiModelProperty(value="经度")
    private String longitude;

    /**
     * 纬度
     */
    @ApiModelProperty(value="纬度")
    private String latitude;

    /**
     * 介绍
     */
    @ApiModelProperty(value="介绍")
    private String introduce;

    /**
     * 图标icon
     */
    @ApiModelProperty(value="图标icon")
    private String icon;


}
