package com.zyw.srsb.bulletin;

import com.zyw.srsb.common.feign.annotation.EnableSrsbFeignClients;
import com.zyw.srsb.common.security.annotation.EnableSrsbResourceServer;
import com.zyw.srsb.common.swagger.annotation.EnableSrsbSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
* @author 朱耀威
* <p>
* 项目启动类
*/
@EnableSrsbSwagger2
@EnableSrsbFeignClients
@EnableSrsbResourceServer
@EnableDiscoveryClient
@SpringBootApplication
public class BulletinApplication {

    public static void main(String[] args) {
        SpringApplication.run(BulletinApplication.class, args);
    }

}
