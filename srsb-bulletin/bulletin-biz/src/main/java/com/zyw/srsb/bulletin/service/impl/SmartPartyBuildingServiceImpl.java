package com.zyw.srsb.bulletin.service.impl;
import com.zyw.srsb.bulletin.dto.response.PartyBasicResponse;
import com.zyw.srsb.bulletin.mapper.SmartPartyBuildingMapper;
import com.zyw.srsb.bulletin.service.SmartPartyBuildingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 智慧党建
 * @author 朱耀威
 */
@Service
public class SmartPartyBuildingServiceImpl implements SmartPartyBuildingService {
	@Autowired
	SmartPartyBuildingMapper smartPartyBuildingMapper;

	@Override
	public PartyBasicResponse queryParyBasic(String villageName) {
		return smartPartyBuildingMapper.queryPartyBasic(villageName);
	}
}
