package com.zyw.srsb.bulletin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.srsb.bulletin.entity.SrsbBulletinSmartCountrysideNews;

/**
 * 智慧农村信息
 *
 * @author 朱耀威
 * @date 2022-05-08 02:05:39
 */
public interface SrsbBulletinSmartCountrysideNewsService extends IService<SrsbBulletinSmartCountrysideNews> {

}
