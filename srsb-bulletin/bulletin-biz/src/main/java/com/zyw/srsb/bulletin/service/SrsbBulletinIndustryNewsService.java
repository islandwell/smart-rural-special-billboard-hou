package com.zyw.srsb.bulletin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.srsb.bulletin.entity.SrsbBulletinIndustryNews;

import java.util.List;

/**
 * 产业新闻
 *
 * @author 朱耀威
 * @date 2022-05-06 14:26:54
 */
public interface SrsbBulletinIndustryNewsService extends IService<SrsbBulletinIndustryNews> {

	List<SrsbBulletinIndustryNews> getNews(Integer num);
}
