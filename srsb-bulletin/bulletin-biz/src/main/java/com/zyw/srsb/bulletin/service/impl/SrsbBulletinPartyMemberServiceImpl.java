package com.zyw.srsb.bulletin.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyw.srsb.bulletin.dto.db.EducationDto;
import com.zyw.srsb.bulletin.dto.db.YearNumDto;
import com.zyw.srsb.bulletin.dto.response.*;
import com.zyw.srsb.bulletin.entity.SrsbBulletinPartyMember;
import com.zyw.srsb.bulletin.mapper.SrsbBulletinPartyMemberMapper;
import com.zyw.srsb.bulletin.service.SrsbBulletinPartyMemberService;
import com.zyw.srsb.bulletin.util.TimeUtils;
import com.zyw.srsb.bulletin.vo.SrsbBulletinPartyMemberVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;

import java.math.BigInteger;
import java.util.*;
import java.util.stream.Collectors;

/**
 * 党员信息表
 *
 * @author 朱耀威
 * @date 2022-01-23 15:58:17
 */
@Service
public class SrsbBulletinPartyMemberServiceImpl extends ServiceImpl<SrsbBulletinPartyMemberMapper, SrsbBulletinPartyMember> implements SrsbBulletinPartyMemberService {

	@Override
	public PartyBranchStatisListResponse queryPartyBranchStatistics() {
		List<PartyBranchStatisResponse> partyBranchStatisResponses = this.baseMapper.queryPartyBranchStatistics();
		PartyBranchStatisListResponse response = new PartyBranchStatisListResponse();
		response.setPartyBranchStatisList(partyBranchStatisResponses);
		return response;
	}

	@Override
	public DataListResponse annualPopulationStatistics() {
		List<TableResponse> tableResponses = this.baseMapper.queryAnnualPopulationStatistics();
		DataListResponse response = new DataListResponse();
		response.setList(tableResponses);
		return response;
	}

	@Override
	public EducationListResponse partyEducationalStatistics() {
		EducationListResponse educationListResponse = new EducationListResponse();
		// 查询学历名称列表
		List<String> educationList = this.baseMapper.queryEducationList();
		Map<String, ArrayList<Integer>> listMap = new LinkedHashMap<>();
		// 如果学历列表不为空
		if (CollectionUtil.isNotEmpty(educationList)){
			educationListResponse.setEducationNameList(educationList);
			// 初步处理学历列表 初始化
			educationList.forEach(r->{
				ArrayList<Integer> tempList = new ArrayList<>();
				tempList.add(0);
				tempList.add(0);
				listMap.put(r,tempList);
			});

			// 查询学历列表
			List<EducationDto> educationDtos = this.baseMapper.queryPartyEducationalStatistics();
			if (CollectionUtil.isNotEmpty(educationDtos)){
				educationDtos.forEach(r->{
					ArrayList<Integer> integers = listMap.get(r.getEducation());
					if ("男".equals(r.getGender())){
						integers.set(0,r.getNo());
					}else{
						integers.set(1,r.getNo());
					}
				});
			}
			List<Integer> maleList = new ArrayList<>();
			List<Integer> femaleList = new ArrayList<>();
			// 设置男女数组
			listMap.entrySet().stream().forEach(r->{
				maleList.add(r.getValue().get(0));
				femaleList.add(r.getValue().get(1));
			});
			educationListResponse.setMaleNoList(maleList);
			educationListResponse.setFemaleList(femaleList);
		}
		return educationListResponse;
	}

	/**
	 * 性别比例
	 *
	 * @return
	 */
	@Override
	public SexRatioResponse querySexRatio() {
		return this.baseMapper.querySexRatio();
	}

	/**
	 * 查询党费交纳记录
	 *
	 * @return
	 */
	@Override
	public PartyFeePaymentRecordListResponse queryPartyFeePaymentRecord() {
		LambdaQueryWrapper<SrsbBulletinPartyMember> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		List<SrsbBulletinPartyMember> srsbBulletinPartyMembers = this.baseMapper.selectList(lambdaQueryWrapper);
		PartyFeePaymentRecordListResponse partyFeePaymentRecordListResponse = new PartyFeePaymentRecordListResponse();
		List<PartyFeePaymentRecordListResponse.PartyFeePaymentRecord> collect = srsbBulletinPartyMembers.stream().map(r -> {
			PartyFeePaymentRecordListResponse.PartyFeePaymentRecord record = new PartyFeePaymentRecordListResponse.PartyFeePaymentRecord();
			record.setPayPartyDues(new BigInteger("" + r.getPayPartyDues()));
			record.setLastPaymentDate(r.getLastPaymentDate());
			record.setFullName(r.getFullName());
			record.setPhoneNo(r.getPhoneNo());
			return record;
		}).collect(Collectors.toList());

		partyFeePaymentRecordListResponse.setPartyFeePaymentRecordList(collect);
		return partyFeePaymentRecordListResponse;

	}

	/**
	 * 党员发展折线图
	 */
	@Override
	public PartyMemberDevelopmentResponse queryPartyMemberDevelopmentResponse() {
		List<YearNumDto> yearNumDtos = this.baseMapper.queryPartyMemberDevelopmentResponse();
		PartyMemberDevelopmentResponse partyMemberDevelopmentResponse = new PartyMemberDevelopmentResponse();
		partyMemberDevelopmentResponse.setYearList(new ArrayList<>());
		partyMemberDevelopmentResponse.setYearNoList(new ArrayList<>());
		partyMemberDevelopmentResponse.setYearTotalNoList(new ArrayList<>());
		if (ObjectUtil.isNotEmpty(yearNumDtos)){
			for (int i = 0;i<yearNumDtos.size();i++){
				YearNumDto r = yearNumDtos.get(i);
				// 年份列表
				partyMemberDevelopmentResponse.getYearList().add(r.getYear());
				// 对应年份入党人数列表
				partyMemberDevelopmentResponse.getYearNoList().add(r.getNum());
				// 党员总人数列表
				if (i == 0) {
					partyMemberDevelopmentResponse.getYearTotalNoList().add(r.getNum());
					continue;
				}
				partyMemberDevelopmentResponse.getYearTotalNoList().add(r.getNum()+partyMemberDevelopmentResponse.getYearTotalNoList().get(i-1));
			}
		}

		return partyMemberDevelopmentResponse;
	}

	/**
	 * 导入党员信息
	 * @param excelVOList
	 * @param bindingResult
	 */
	@Transactional(rollbackFor = Exception.class)
    @Override
    public void importSrsbBulletinPartyMember(List<SrsbBulletinPartyMemberVo> excelVOList, BindingResult bindingResult) {
		List<SrsbBulletinPartyMember> collect = excelVOList.stream().map(r -> {
			SrsbBulletinPartyMember item = new SrsbBulletinPartyMember();
			item.setPartyOrganization(r.getPartyOrganization());
			item.setCode(r.getCode());
			item.setFullName(r.getFullName());
			item.setCardNo(r.getCardNo());
			item.setPhoneNo(r.getPhoneNo());
			item.setPayPartyDues(r.getPayPartyDues());
			item.setGender(r.getGender());
			item.setAge(r.getAge());
			item.setPartyAge(r.getPartyAge());
			item.setEducation(r.getEducation());
			item.setWorkingTime(TimeUtils.stringToLocalDateTime(r.getWorkingTime()));
			item.setNation(r.getNation());
			item.setBirthday(TimeUtils.stringToLocalDateTime(r.getBirthday()));
			item.setJoinDate(TimeUtils.stringToLocalDateTime(r.getJoinDate()));
			item.setMajor(r.getMajor());
			item.setEmployer(r.getEmployer());
			item.setLastPaymentDate(TimeUtils.stringToLocalDateTime(r.getLastPaymentDate()));
			return item;
		}).collect(Collectors.toList());
		this.saveOrUpdateBatch(collect);
    }

	/**
	 * 导出党员信息
	 *
	 * @return
	 */
	@Override
	public List<ExportSrsbBulletinPartyMemberResponse> listSrsbBulletinPartyMember() {
		List<SrsbBulletinPartyMember> list = this.list();
		List<ExportSrsbBulletinPartyMemberResponse> collect = list.stream().map(r -> {
			ExportSrsbBulletinPartyMemberResponse item = new ExportSrsbBulletinPartyMemberResponse();
			item.setId(r.getId());
			item.setPartyOrganization(r.getPartyOrganization());
			item.setCode(r.getCode());
			item.setFullName(r.getFullName());
			item.setCardNo(r.getCardNo());
			item.setPhoneNo(r.getPhoneNo());
			item.setPayPartyDues(r.getPayPartyDues());
			item.setGender(r.getGender());
			item.setAge(r.getAge());
			item.setPartyAge(r.getPartyAge());
			item.setEducation(r.getEducation());
			item.setWorkingTime(r.getWorkingTime());
			item.setNation(r.getNation());
			item.setBirthday(r.getBirthday());
			item.setJoinDate(r.getJoinDate());
			item.setMajor(r.getMajor());
			item.setEmployer(r.getEmployer());
			item.setLastPaymentDate(r.getLastPaymentDate());
			item.setCreateBy(r.getCreateBy());
			item.setCreateTime(r.getCreateTime());
			item.setUpdateBy(r.getUpdateBy());
			item.setUpdateTime(r.getUpdateTime());
			return item;
		}).collect(Collectors.toList());
		return collect;
	}
}
