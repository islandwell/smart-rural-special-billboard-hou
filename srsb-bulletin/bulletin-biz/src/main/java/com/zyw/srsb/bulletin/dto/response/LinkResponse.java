package com.zyw.srsb.bulletin.dto.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 外链传输类
 * @author 朱耀威
 */
@Data
@ApiModel(value = "外链传输类")
public class LinkResponse {
	@ApiModelProperty(value="url")
	private String url;

	@ApiModelProperty(value="密钥")
	private String SecrecyKey;

	@ApiModelProperty(value="失效时间")
	private LocalDateTime endTime;
}
