package com.zyw.srsb.bulletin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyw.srsb.bulletin.dto.event.GetKanbanCropListEvent;
import com.zyw.srsb.bulletin.entity.SrsbBulletinCrop;
import com.zyw.srsb.bulletin.service.SrsbBulletinCropService;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 农作物
 *
 * @author 朱耀威
 * @date 2022-05-04 16:59:30
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletincrop")
@Api(value = "srsbbulletincrop", tags = "农作物管理")
public class SrsbBulletinCropController {

	private final SrsbBulletinCropService srsbBulletinCropService;

	/**
	 * 分页查询
	 *
	 * @param page             分页对象
	 * @param srsbBulletinCrop 农作物
	 * @return
	 */
	@ApiOperation(value = "分页查询", notes = "分页查询")
	@GetMapping("/page")
	@PreAuthorize("@pms.hasPermission('bulletin_srsbbulletincrop_get')")
	public R getSrsbBulletinCropPage(Page page, SrsbBulletinCrop srsbBulletinCrop) {
		return R.ok(srsbBulletinCropService.page(page, Wrappers.query(srsbBulletinCrop)));
	}


	/**
	 * 通过id查询农作物
	 *
	 * @param id id
	 * @return R
	 */
	@ApiOperation(value = "通过id查询", notes = "通过id查询")
	@GetMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('bulletin_srsbbulletincrop_get')")
	public R getById(@PathVariable("id") Integer id) {
		return R.ok(srsbBulletinCropService.getById(id));
	}

	/**
	 * 新增农作物
	 *
	 * @param srsbBulletinCrop 农作物
	 * @return R
	 */
	@ApiOperation(value = "新增农作物", notes = "新增农作物")
	@SysLog("新增农作物")
	@PostMapping
	@PreAuthorize("@pms.hasPermission('bulletin_srsbbulletincrop_add')")
	public R save(@RequestBody SrsbBulletinCrop srsbBulletinCrop) {
		return R.ok(srsbBulletinCropService.save(srsbBulletinCrop));
	}

	/**
	 * 修改农作物
	 *
	 * @param srsbBulletinCrop 农作物
	 * @return R
	 */
	@ApiOperation(value = "修改农作物", notes = "修改农作物")
	@SysLog("修改农作物")
	@PutMapping
	@PreAuthorize("@pms.hasPermission('bulletin_srsbbulletincrop_edit')")
	public R updateById(@RequestBody SrsbBulletinCrop srsbBulletinCrop) {
		return R.ok(srsbBulletinCropService.updateById(srsbBulletinCrop));
	}

	/**
	 * 通过id删除农作物
	 *
	 * @param id id
	 * @return R
	 */
	@ApiOperation(value = "通过id删除农作物", notes = "通过id删除农作物")
	@SysLog("通过id删除农作物")
	@DeleteMapping("/{id}")
	@PreAuthorize("@pms.hasPermission('bulletin_srsbbulletincrop_del')")
	public R removeById(@PathVariable Integer id) {
		return R.ok(srsbBulletinCropService.removeById(id));
	}

	/**
	 * 看板农作物列表
	 *
	 * @return
	 */
	@PostMapping("/getKanbanCropList")
	@Inner(value = false)
	public R<List<GetKanbanCropListEvent>> getKanbanCropList() {
		return R.ok(srsbBulletinCropService.getMerchantOperation());
	}

}
