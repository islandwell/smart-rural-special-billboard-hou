package com.zyw.srsb.bulletin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyw.srsb.bulletin.dto.event.GetKanbanCropListEvent;
import com.zyw.srsb.bulletin.entity.SrsbBulletinCrop;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.math.BigInteger;
import java.util.List;

/**
 * 农作物
 *
 * @author 朱耀威
 * @date 2022-05-04 16:59:30
 */
@Mapper
public interface SrsbBulletinCropMapper extends BaseMapper<SrsbBulletinCrop> {
	@Select("select count(DISTINCT name) as typeNum from srsb_bulletin_crop")
	BigInteger tongji1();

	@Select("select name,status as value from srsb_bulletin_crop")
	List<GetKanbanCropListEvent> getMerchantOperation();
}
