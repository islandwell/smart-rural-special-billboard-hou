package com.zyw.srsb.bulletin.controller;

import com.alibaba.nacos.shaded.com.google.j2objc.annotations.RetainedWith;
import com.zyw.srsb.bulletin.dto.event.GeoV2Event;
import com.zyw.srsb.bulletin.dto.response.GeoResponse;
import com.zyw.srsb.bulletin.service.GaodeService;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * 看板权限
 *
 * @author 朱耀威
 * @date 2022-01-23 13:56:27
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/authBulletin")
@Api(value = "authBulletin", tags = "看板权限")
public class AuthBulletinController {
	private final GaodeService gaodeService;

	/**
	 * 获取地点坐标和坐标系
	 * @param event
	 * @return
	 */
	@Inner(value = false)
	@PostMapping("/getGeoLocation")
	public R<GeoResponse> getGeoLocation(@RequestBody GeoV2Event event){
		return R.ok(gaodeService.getGeoLocation(event));
	}
}
