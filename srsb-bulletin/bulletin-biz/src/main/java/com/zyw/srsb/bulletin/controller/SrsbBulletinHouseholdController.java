package com.zyw.srsb.bulletin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyw.srsb.bulletin.dto.event.GetBaseInfoEvent;
import com.zyw.srsb.bulletin.dto.event.MerchantInfoEvent;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.bulletin.entity.SrsbBulletinHousehold;
import com.zyw.srsb.bulletin.service.SrsbBulletinHouseholdService;
import com.zyw.srsb.common.security.annotation.Inner;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 农户
 *
 * @author 朱耀威
 * @date 2022-05-04 16:29:27
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletinhousehold" )
@Api(value = "srsbbulletinhousehold", tags = "农户管理")
public class SrsbBulletinHouseholdController {

    private final  SrsbBulletinHouseholdService srsbBulletinHouseholdService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param srsbBulletinHousehold 农户
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinhousehold_get')" )
    public R getSrsbBulletinHouseholdPage(Page page, SrsbBulletinHousehold srsbBulletinHousehold) {
        return R.ok(srsbBulletinHouseholdService.page(page, Wrappers.query(srsbBulletinHousehold)));
    }


    /**
     * 通过id查询农户
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinhousehold_get')" )
    public R getById(@PathVariable("id" ) Integer id) {
        return R.ok(srsbBulletinHouseholdService.getById(id));
    }

    /**
     * 新增农户
     * @param srsbBulletinHousehold 农户
     * @return R
     */
    @ApiOperation(value = "新增农户", notes = "新增农户")
    @SysLog("新增农户" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinhousehold_add')" )
    public R save(@RequestBody SrsbBulletinHousehold srsbBulletinHousehold) {
        return R.ok(srsbBulletinHouseholdService.save(srsbBulletinHousehold));
    }

    /**
     * 修改农户
     * @param srsbBulletinHousehold 农户
     * @return R
     */
    @ApiOperation(value = "修改农户", notes = "修改农户")
    @SysLog("修改农户" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinhousehold_edit')" )
    public R updateById(@RequestBody SrsbBulletinHousehold srsbBulletinHousehold) {
        return R.ok(srsbBulletinHouseholdService.updateById(srsbBulletinHousehold));
    }

    /**
     * 通过id删除农户
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除农户", notes = "通过id删除农户")
    @SysLog("通过id删除农户" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinhousehold_del')" )
    public R removeById(@PathVariable Integer id) {
        return R.ok(srsbBulletinHouseholdService.removeById(id));
    }

	/**
	 * 看板获取商户信息
	 * @return
	 */
	@PostMapping("/getBaseInfo")
	@ApiOperation(value = "看板获取商户信息",notes = "看板获取商户信息")
	@Inner(value = false)
	public R<GetBaseInfoEvent> getBaseInfo(){
		return R.ok(srsbBulletinHouseholdService.getBaseInfo());
	}
}
