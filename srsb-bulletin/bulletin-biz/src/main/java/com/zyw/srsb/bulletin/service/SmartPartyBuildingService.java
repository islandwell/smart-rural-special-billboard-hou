package com.zyw.srsb.bulletin.service;

import com.zyw.srsb.bulletin.dto.response.PartyBasicResponse;

/**
 * 智慧党建业务逻辑
 *
 * @author 朱耀威
 * @date 2022-01-23 13:56:27
 */
public interface SmartPartyBuildingService {
	public PartyBasicResponse queryParyBasic(String villageName);
}
