/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the srsb4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.zyw.srsb.bulletin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.srsb.bulletin.dto.response.*;
import com.zyw.srsb.bulletin.entity.SrsbBulletinPartyMember;
import com.zyw.srsb.bulletin.vo.SrsbBulletinPartyMemberVo;
import org.springframework.validation.BindingResult;

import java.util.List;

/**
 * 党员信息表
 *
 * @author 朱耀威
 * @date 2022-01-23 15:58:17
 */
public interface SrsbBulletinPartyMemberService extends IService<SrsbBulletinPartyMember> {

	/**
	 * 统计各个党支部人数
	 * @return
	 */
	PartyBranchStatisListResponse queryPartyBranchStatistics();

	/**
	 * 统计各个年份人数
	 * @return
	 */
	DataListResponse annualPopulationStatistics();

	/**
	 * 党员学历统计
	 * @return
	 */
	EducationListResponse partyEducationalStatistics();

	/**
	 * 性别比例
	 * @return
	 */
	SexRatioResponse querySexRatio();

	/**
	 * 查询党费交纳记录
	 * @return
	 */
	PartyFeePaymentRecordListResponse queryPartyFeePaymentRecord();

	/**
	 * 党员发展折线图
	 */
	PartyMemberDevelopmentResponse queryPartyMemberDevelopmentResponse();

	/**
	 * 导入党员信息
	 * @param excelVOList
	 * @param bindingResult
	 */
	void importSrsbBulletinPartyMember(List<SrsbBulletinPartyMemberVo> excelVOList, BindingResult bindingResult);

	/**
	 * 导出党员信息
	 * @return
	 */
	List<ExportSrsbBulletinPartyMemberResponse> listSrsbBulletinPartyMember();
}
