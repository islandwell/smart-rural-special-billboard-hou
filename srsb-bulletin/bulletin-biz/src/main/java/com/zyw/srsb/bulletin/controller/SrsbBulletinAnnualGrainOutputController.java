package com.zyw.srsb.bulletin.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyw.srsb.bulletin.entity.SrsbBulletinAgricultural;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.bulletin.entity.SrsbBulletinAnnualGrainOutput;
import com.zyw.srsb.bulletin.service.SrsbBulletinAnnualGrainOutputService;
import com.zyw.srsb.common.security.annotation.Inner;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 粮食年产量表
 *
 * @author 朱耀威
 * @date 2022-04-04 17:00:19
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletinannualgrainoutput" )
@Api(value = "srsbbulletinannualgrainoutput", tags = "粮食年产量表管理")
public class SrsbBulletinAnnualGrainOutputController {

    private final  SrsbBulletinAnnualGrainOutputService srsbBulletinAnnualGrainOutputService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param srsbBulletinAnnualGrainOutput 粮食年产量表
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinannualgrainoutput_get')" )
    public R getSrsbBulletinAnnualGrainOutputPage(Page page, SrsbBulletinAnnualGrainOutput srsbBulletinAnnualGrainOutput) {
        return R.ok(srsbBulletinAnnualGrainOutputService.page(page, Wrappers.query(srsbBulletinAnnualGrainOutput)));
    }


    /**
     * 通过id查询粮食年产量表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinannualgrainoutput_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(srsbBulletinAnnualGrainOutputService.getById(id));
    }

    /**
     * 新增粮食年产量表
     * @param srsbBulletinAnnualGrainOutput 粮食年产量表
     * @return R
     */
    @ApiOperation(value = "新增粮食年产量表", notes = "新增粮食年产量表")
    @SysLog("新增粮食年产量表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinannualgrainoutput_add')" )
    public R save(@RequestBody SrsbBulletinAnnualGrainOutput srsbBulletinAnnualGrainOutput) {
        return R.ok(srsbBulletinAnnualGrainOutputService.save(srsbBulletinAnnualGrainOutput));
    }

    /**
     * 修改粮食年产量表
     * @param srsbBulletinAnnualGrainOutput 粮食年产量表
     * @return R
     */
    @ApiOperation(value = "修改粮食年产量表", notes = "修改粮食年产量表")
    @SysLog("修改粮食年产量表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinannualgrainoutput_edit')" )
    public R updateById(@RequestBody SrsbBulletinAnnualGrainOutput srsbBulletinAnnualGrainOutput) {
        return R.ok(srsbBulletinAnnualGrainOutputService.updateById(srsbBulletinAnnualGrainOutput));
    }

    /**
     * 通过id删除粮食年产量表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除粮食年产量表", notes = "通过id删除粮食年产量表")
    @SysLog("通过id删除粮食年产量表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinannualgrainoutput_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(srsbBulletinAnnualGrainOutputService.removeById(id));
    }

	@Inner(value = false)
	@PostMapping("/getLiangShiData/{year}")
	@ApiOperation(value = "获取粮食产值数据", notes = "获取粮食产值数据")
	public R<SrsbBulletinAnnualGrainOutput> getLiangShiData(@PathVariable String year){
		LambdaQueryWrapper<SrsbBulletinAnnualGrainOutput> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(SrsbBulletinAnnualGrainOutput::getParticularYear,year).last("limit 1");
		List<SrsbBulletinAnnualGrainOutput> list = srsbBulletinAnnualGrainOutputService.list(queryWrapper);
		if (CollectionUtil.isNotEmpty(list)){
			return R.ok(list.get(0));
		}
		return R.ok();
	}

}
