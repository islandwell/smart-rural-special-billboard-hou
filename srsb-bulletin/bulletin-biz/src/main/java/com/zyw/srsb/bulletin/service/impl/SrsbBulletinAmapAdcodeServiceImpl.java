package com.zyw.srsb.bulletin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyw.srsb.bulletin.entity.SrsbBulletinAmapAdcode;
import com.zyw.srsb.bulletin.mapper.SrsbBulletinAmapAdcodeMapper;
import com.zyw.srsb.bulletin.service.SrsbBulletinAmapAdcodeService;
import org.springframework.stereotype.Service;

/**
 * 地理编码表
 *
 * @author 朱耀威
 * @date 2022-04-04 23:13:33
 */
@Service
public class SrsbBulletinAmapAdcodeServiceImpl extends ServiceImpl<SrsbBulletinAmapAdcodeMapper, SrsbBulletinAmapAdcode> implements SrsbBulletinAmapAdcodeService {

}
