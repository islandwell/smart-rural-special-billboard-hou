package com.zyw.srsb.bulletin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyw.srsb.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 产业新闻
 *
 * @author 朱耀威
 * @date 2022-05-06 14:26:54
 */
@Data
@TableName("srsb_bulletin_industry_news")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "产业新闻")
public class SrsbBulletinIndustryNews extends BaseEntity {

    /**
     * 主键id
     */
    @TableId
    @ApiModelProperty(value="主键id")
    private Integer id;

    /**
     * 新闻标题
     */
    @ApiModelProperty(value="新闻标题")
    private String newsTitle;

    /**
     * 父标题
     */
    @ApiModelProperty(value="父标题")
    private String subTitle;

    /**
     * 作者
     */
    @ApiModelProperty(value="作者")
    private String author;

    /**
     * 标签
     */
    @ApiModelProperty(value="标签")
    private String labels;

    /**
     * 新闻内容
     */
    @ApiModelProperty(value="新闻内容")
    private String newContent;

    /**
     * 简介
     */
    @ApiModelProperty(value="简介")
    private String briefIntroduction;


}
