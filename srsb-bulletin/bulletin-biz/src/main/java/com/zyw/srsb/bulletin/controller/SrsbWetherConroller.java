package com.zyw.srsb.bulletin.controller;

import com.zyw.srsb.bulletin.dto.response.WetherResponse;
import com.zyw.srsb.bulletin.service.SrsbWeatherService;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.security.annotation.Inner;
import com.zyw.srsb.datacollect.dto.event.WeatherEvent;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbwether" )
@Api(value = "srsbwether", tags = "天气控制器")
public class SrsbWetherConroller {
	@Autowired
	SrsbWeatherService srsbWetherService;

	@Value("${qweather.baseUrl}")
	private String baseUrl;

	@Value("${qweather.key}")
	private String key;

	@Value("${qweather.location}")
	private String location;

	@PostMapping("/weatherInformation")
	@Inner(value = false)
	public R<WetherResponse> weatherInformation(){
		WeatherEvent event = new WeatherEvent();
		event.setKey(key);
		event.setLocation(location);
		event.setBaseUrl(baseUrl);
		WetherResponse wetherResponse = srsbWetherService.queryWeaterInformation(event);
		return R.ok(wetherResponse);
	}

}
