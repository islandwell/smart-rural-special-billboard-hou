package com.zyw.srsb.bulletin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.srsb.bulletin.dto.event.GetKanbanCropListEvent;
import com.zyw.srsb.bulletin.entity.SrsbBulletinCrop;

import java.util.List;

/**
 * 农作物
 *
 * @author 朱耀威
 * @date 2022-05-04 16:59:30
 */
public interface SrsbBulletinCropService extends IService<SrsbBulletinCrop> {

	/**
	 * 看板作物列表
	 * @return
	 */
	List<GetKanbanCropListEvent> getMerchantOperation();
}
