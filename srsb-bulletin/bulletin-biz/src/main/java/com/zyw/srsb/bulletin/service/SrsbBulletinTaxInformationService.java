package com.zyw.srsb.bulletin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.srsb.bulletin.entity.SrsbBulletinTaxInformation;

/**
 * 纳税信息表
 *
 * @author 朱耀威
 * @date 2022-05-04 18:24:20
 */
public interface SrsbBulletinTaxInformationService extends IService<SrsbBulletinTaxInformation> {

}
