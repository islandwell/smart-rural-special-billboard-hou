/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the srsb4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.zyw.srsb.bulletin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyw.srsb.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 看板
 *
 * @author 朱耀威
 * @date 2022-01-23 13:56:27
 */
@Data
@TableName("srsb_bulletin_board")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "看板")
public class SrsbBulletinBoard extends BaseEntity {

    /**
     * 主键id
     */
    @TableId
    @ApiModelProperty(value="主键id")
    private Long id;

    /**
     * 看板名称
     */
    @ApiModelProperty(value="看板名称")
    private String name;

    /**
     * 封面url(图片url)
     */
    @ApiModelProperty(value="封面url(图片url)")
    private String coverUrl;

    /**
     * 介绍
     */
    @ApiModelProperty(value="介绍")
    private String introduce;

    /**
     * 发布状态 0-未发布 1-发布
     */
    @ApiModelProperty(value="发布状态 0-未发布 1-发布")
    private String status;

	/**
	 * 应用url
	 */
	@ApiModelProperty(value="应用url")
	private String baseUrl;


}
