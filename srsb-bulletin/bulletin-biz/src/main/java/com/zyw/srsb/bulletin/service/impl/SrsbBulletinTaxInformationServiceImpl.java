package com.zyw.srsb.bulletin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyw.srsb.bulletin.entity.SrsbBulletinTaxInformation;
import com.zyw.srsb.bulletin.mapper.SrsbBulletinTaxInformationMapper;
import com.zyw.srsb.bulletin.service.SrsbBulletinTaxInformationService;
import org.springframework.stereotype.Service;

/**
 * 纳税信息表
 *
 * @author 朱耀威
 * @date 2022-05-04 18:24:20
 */
@Service
public class SrsbBulletinTaxInformationServiceImpl extends ServiceImpl<SrsbBulletinTaxInformationMapper, SrsbBulletinTaxInformation> implements SrsbBulletinTaxInformationService {

}
