package com.zyw.srsb.bulletin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyw.srsb.bulletin.entity.SrsbBulletinAmapAdcode;
import org.apache.ibatis.annotations.Mapper;

/**
 * 地理编码表
 *
 * @author 朱耀威
 * @date 2022-04-04 23:13:33
 */
@Mapper
public interface SrsbBulletinAmapAdcodeMapper extends BaseMapper<SrsbBulletinAmapAdcode> {

}
