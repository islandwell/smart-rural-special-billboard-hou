package com.zyw.srsb.bulletin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyw.srsb.bulletin.entity.SrsbBulletinSmartCountrysideNews;
import com.zyw.srsb.bulletin.mapper.SrsbBulletinSmartCountrysideNewsMapper;
import com.zyw.srsb.bulletin.service.SrsbBulletinSmartCountrysideNewsService;
import org.springframework.stereotype.Service;

/**
 * 智慧农村信息
 *
 * @author 朱耀威
 * @date 2022-05-08 02:05:39
 */
@Service
public class SrsbBulletinSmartCountrysideNewsServiceImpl extends ServiceImpl<SrsbBulletinSmartCountrysideNewsMapper, SrsbBulletinSmartCountrysideNews> implements SrsbBulletinSmartCountrysideNewsService {

}
