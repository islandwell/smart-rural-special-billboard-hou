package com.zyw.srsb.bulletin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyw.srsb.bulletin.dto.event.IsAuthEvent;
import com.zyw.srsb.bulletin.dto.event.LinkEvent;
import com.zyw.srsb.bulletin.dto.response.LinkResponse;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.bulletin.entity.SrsbBulletinLinkList;
import com.zyw.srsb.bulletin.service.SrsbBulletinLinkListService;
import com.zyw.srsb.common.security.annotation.Inner;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * 看板外链
 *
 * @author 朱耀威
 * @date 2022-01-23 14:53:40
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletinlinklist" )
@Api(value = "srsbbulletinlinklist", tags = "看板外链管理")
public class SrsbBulletinLinkListController {

    private final  SrsbBulletinLinkListService srsbBulletinLinkListService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param srsbBulletinLinkList 看板外链
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinlinklist_get')" )
    public R getSrsbBulletinLinkListPage(Page page, SrsbBulletinLinkList srsbBulletinLinkList) {
        return R.ok(srsbBulletinLinkListService.page(page, Wrappers.query(srsbBulletinLinkList)));
    }


    /**
     * 通过id查询看板外链
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinlinklist_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(srsbBulletinLinkListService.getById(id));
    }

    /**
     * 新增看板外链
     * @param srsbBulletinLinkList 看板外链
     * @return R
     */
    @ApiOperation(value = "新增看板外链", notes = "新增看板外链")
    @SysLog("新增看板外链" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinlinklist_add')" )
    public R save(@RequestBody SrsbBulletinLinkList srsbBulletinLinkList) {
        return R.ok(srsbBulletinLinkListService.save(srsbBulletinLinkList));
    }

    /**
     * 修改看板外链
     * @param srsbBulletinLinkList 看板外链
     * @return R
     */
    @ApiOperation(value = "修改看板外链", notes = "修改看板外链")
    @SysLog("修改看板外链" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinlinklist_edit')" )
    public R updateById(@RequestBody SrsbBulletinLinkList srsbBulletinLinkList) {
        return R.ok(srsbBulletinLinkListService.updateById(srsbBulletinLinkList));
    }

    /**
     * 通过id删除看板外链
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除看板外链", notes = "通过id删除看板外链")
    @SysLog("通过id删除看板外链" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinlinklist_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(srsbBulletinLinkListService.removeById(id));
    }

	/**
	 * 创建外链
	 * @param event
	 * @return R
	 */
	@ApiOperation(value = "创建外链", notes = "创建外链")
	@SysLog("创建外链" )
	@PostMapping("/create" )
	@PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinlinklist_create')" )
	public R<LinkResponse> createLink(@RequestBody LinkEvent event) {
		return R.ok(srsbBulletinLinkListService.createLink(event));
	}

	/**
	 * 外链鉴权
	 * @param secretKey
	 * @return R
	 */
	@ApiOperation(value = "外链鉴权", notes = "外链鉴权")
	@SysLog("外链鉴权" )
	@PostMapping("/auth" )
	@Inner(value = false)
	public R isAuth(@RequestBody IsAuthEvent event, HttpServletRequest request) {
		srsbBulletinLinkListService.isAuth(event,request);
		return R.ok();
	}
}
