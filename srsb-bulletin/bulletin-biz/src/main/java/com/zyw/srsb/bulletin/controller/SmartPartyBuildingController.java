package com.zyw.srsb.bulletin.controller;

import com.zyw.srsb.bulletin.dto.response.PartyBasicResponse;
import com.zyw.srsb.bulletin.service.SmartPartyBuildingService;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 智慧党建看板控制器
 *
 * @author 朱耀威
 * @date 2022-01-23 13:56:27
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/smartpartybuildingcontroller" )
@Api(value = "smartpartybuildingcontroller", tags = "智慧党建")
public class SmartPartyBuildingController {
	@Autowired
	SmartPartyBuildingService smartPartyBuildingService;

	/**
	 * 查询党建基础信息
	 * @param villageName
	 * @return
	 */
	@Inner(value = false)
	@GetMapping("/queryPartyBasicInformation/{villageName}")
	public R<PartyBasicResponse> getPartyBasicInformation(@PathVariable(value = "villageName") String villageName){
		return R.ok(smartPartyBuildingService.queryParyBasic(villageName));
	}
}
