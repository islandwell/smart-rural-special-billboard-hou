package com.zyw.srsb.bulletin.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyw.srsb.bulletin.entity.SrsbBulletinVillageInformation;
import com.zyw.srsb.bulletin.mapper.SrsbBulletinVillageInformationMapper;
import com.zyw.srsb.bulletin.service.SrsbBulletinVillageInformationService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 村庄信息表
 *
 * @author 朱耀威
 * @date 2022-03-27 14:51:01
 */
@Service
public class SrsbBulletinVillageInformationServiceImpl extends ServiceImpl<SrsbBulletinVillageInformationMapper, SrsbBulletinVillageInformation> implements SrsbBulletinVillageInformationService {

	@Override
	public SrsbBulletinVillageInformation findSrsbBulletinVillageInformation(String name) {
		LambdaQueryWrapper<SrsbBulletinVillageInformation> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(SrsbBulletinVillageInformation::getName,name).last("limit 1");
		List<SrsbBulletinVillageInformation> list = this.list(queryWrapper);
		if (CollectionUtil.isNotEmpty(list)){
			return list.get(0);
		}
		return null;
	}
}
