package com.zyw.srsb.bulletin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.bulletin.entity.SrsbBulletinLoginRecord;
import com.zyw.srsb.bulletin.service.SrsbBulletinLoginRecordService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 看板登陆记录表
 *
 * @author 朱耀威
 * @date 2022-01-23 15:05:57
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletinloginrecord" )
@Api(value = "srsbbulletinloginrecord", tags = "看板登陆记录表管理")
public class SrsbBulletinLoginRecordController {

    private final  SrsbBulletinLoginRecordService srsbBulletinLoginRecordService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param srsbBulletinLoginRecord 看板登陆记录表
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinloginrecord_get')" )
    public R getSrsbBulletinLoginRecordPage(Page page, SrsbBulletinLoginRecord srsbBulletinLoginRecord) {
        return R.ok(srsbBulletinLoginRecordService.page(page, Wrappers.query(srsbBulletinLoginRecord)));
    }


    /**
     * 通过id查询看板登陆记录表
     * @param   id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{  id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinloginrecord_get')" )
    public R getById(@PathVariable("  id" ) Long   id) {
        return R.ok(srsbBulletinLoginRecordService.getById(  id));
    }

    /**
     * 新增看板登陆记录表
     * @param srsbBulletinLoginRecord 看板登陆记录表
     * @return R
     */
    @ApiOperation(value = "新增看板登陆记录表", notes = "新增看板登陆记录表")
    @SysLog("新增看板登陆记录表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinloginrecord_add')" )
    public R save(@RequestBody SrsbBulletinLoginRecord srsbBulletinLoginRecord) {
        return R.ok(srsbBulletinLoginRecordService.save(srsbBulletinLoginRecord));
    }

    /**
     * 修改看板登陆记录表
     * @param srsbBulletinLoginRecord 看板登陆记录表
     * @return R
     */
    @ApiOperation(value = "修改看板登陆记录表", notes = "修改看板登陆记录表")
    @SysLog("修改看板登陆记录表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinloginrecord_edit')" )
    public R updateById(@RequestBody SrsbBulletinLoginRecord srsbBulletinLoginRecord) {
        return R.ok(srsbBulletinLoginRecordService.updateById(srsbBulletinLoginRecord));
    }

    /**
     * 通过id删除看板登陆记录表
     * @param   id id
     * @return R
     */
    @ApiOperation(value = "通过id删除看板登陆记录表", notes = "通过id删除看板登陆记录表")
    @SysLog("通过id删除看板登陆记录表" )
    @DeleteMapping("/{  id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinloginrecord_del')" )
    public R removeById(@PathVariable Long   id) {
        return R.ok(srsbBulletinLoginRecordService.removeById(  id));
    }

}
