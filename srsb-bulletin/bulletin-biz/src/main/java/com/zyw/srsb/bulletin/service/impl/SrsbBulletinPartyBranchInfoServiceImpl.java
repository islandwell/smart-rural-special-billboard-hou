package com.zyw.srsb.bulletin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyw.srsb.bulletin.dto.response.AllPartyBranchResponse;
import com.zyw.srsb.bulletin.entity.SrsbBulletinPartyBranchInfo;
import com.zyw.srsb.bulletin.mapper.SrsbBulletinPartyBranchInfoMapper;
import com.zyw.srsb.bulletin.service.SrsbBulletinPartyBranchInfoService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 党支部管理
 *
 * @author 朱耀威
 * @date 2022-04-09 20:28:21
 */
@Service
public class SrsbBulletinPartyBranchInfoServiceImpl extends ServiceImpl<SrsbBulletinPartyBranchInfoMapper, SrsbBulletinPartyBranchInfo> implements SrsbBulletinPartyBranchInfoService {

	@Override
	public List<AllPartyBranchResponse> getAllPartyBranchList() {
		LambdaQueryWrapper<SrsbBulletinPartyBranchInfo> lambdaQueryWrapper = new LambdaQueryWrapper<>();
		lambdaQueryWrapper.eq(SrsbBulletinPartyBranchInfo::getStatus,1);
		List<SrsbBulletinPartyBranchInfo> list = this.list(lambdaQueryWrapper);
		List<AllPartyBranchResponse> collect = list.stream().map(r -> {
			AllPartyBranchResponse item = new AllPartyBranchResponse();
			item.setId(r.getId());
			item.setName(r.getName());
			item.setAddress(r.getAddress());
			item.setLongitude(r.getLongitude());
			item.setLatitude(r.getLatitude());
			item.setPointSystem(r.getPointSystem());
			item.setContext(r.getContext());
			item.setEstablishmentTime(r.getEstablishmentTime());
			item.setStatus(r.getStatus());
			return item;
		}).collect(Collectors.toList());
		return collect;
	}
}
