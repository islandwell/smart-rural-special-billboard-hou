/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the srsb4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.zyw.srsb.bulletin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.srsb.bulletin.dto.event.IsAuthEvent;
import com.zyw.srsb.bulletin.dto.event.LinkEvent;
import com.zyw.srsb.bulletin.dto.response.LinkResponse;
import com.zyw.srsb.bulletin.entity.SrsbBulletinLinkList;

import javax.servlet.http.HttpServletRequest;

/**
 * 看板外链
 *
 * @author 朱耀威
 * @date 2022-01-23 14:53:40
 */
public interface SrsbBulletinLinkListService extends IService<SrsbBulletinLinkList> {

	/**
	 * 创建外链
	 * @param event
	 * @return
	 */
	LinkResponse createLink(LinkEvent event);

	void isAuth(IsAuthEvent event, HttpServletRequest request);
}
