package com.zyw.srsb.bulletin.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyw.srsb.bulletin.dto.db.TaxInfoDto;
import com.zyw.srsb.bulletin.dto.event.MerchantDataEvent;
import com.zyw.srsb.bulletin.dto.event.MerchantInfoEvent;
import com.zyw.srsb.bulletin.dto.response.SrsbBulletinMerchantInformationResponse;
import com.zyw.srsb.bulletin.entity.SrsbBulletinMerchantInformation;
import com.zyw.srsb.bulletin.mapper.SrsbBulletinMerchantInformationMapper;
import com.zyw.srsb.bulletin.mapper.SrsbBulletinTaxInformationMapper;
import com.zyw.srsb.bulletin.service.SrsbBulletinMerchantInformationService;
import com.zyw.srsb.bulletin.util.TimeUtils;
import com.zyw.srsb.bulletin.vo.SrsbBulletinMerchantInformationVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 商户信息
 *
 * @author 朱耀威
 * @date 2022-02-13 13:48:20
 */
@Service
public class SrsbBulletinMerchantInformationServiceImpl extends ServiceImpl<SrsbBulletinMerchantInformationMapper, SrsbBulletinMerchantInformation> implements SrsbBulletinMerchantInformationService {

	@Autowired
	SrsbBulletinTaxInformationMapper srsbBulletinTaxInformationMapper;
	/**
	 * 导入商户信息
	 *
	 * @param excelVOList
	 * @param bindingResult
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public void importSrsbBulletinMerchantInformation(List<SrsbBulletinMerchantInformationVo> excelVOList, BindingResult bindingResult) {
		List<SrsbBulletinMerchantInformation> collect = excelVOList.stream().map(r -> {
			SrsbBulletinMerchantInformation item = new SrsbBulletinMerchantInformation();
			item.setBusinessType(r.getBusinessType());
			item.setCode(r.getCode());
			item.setName(r.getName());
			item.setAddress(r.getAddress());
			item.setPoint(r.getPoint());
			item.setContacts(r.getContacts());
			item.setOperatingTime(TimeUtils.stringToLocalDateTime(r.getOperatingTime()));
			item.setBusinessNature(r.getBusinessNature());
			item.setIntroduction(r.getIntroduction());
			item.setMerchantDetails(r.getMerchantDetails());
			item.setRemarks(r.getRemarks());
			return item;
		}).collect(Collectors.toList());
		this.saveOrUpdateBatch(collect);
	}

	/**
	 * 导出商户信息
	 *
	 * @return
	 */
	@Override
	public List<SrsbBulletinMerchantInformationResponse> export() {
		List<SrsbBulletinMerchantInformation> list = this.list();
		return list.stream().map(r -> {
			SrsbBulletinMerchantInformationResponse item = new SrsbBulletinMerchantInformationResponse();
			item.setId(r.getId());
			item.setBusinessType(r.getBusinessType());
			item.setCode(r.getCode());
			item.setName(r.getName());
			item.setAddress(r.getAddress());
			item.setPoint(r.getPoint());
			item.setContacts(r.getContacts());
			item.setOperatingTime(r.getOperatingTime());
			item.setBusinessNature(r.getBusinessNature());
			item.setIntroduction(r.getIntroduction());
			item.setMerchantDetails(r.getMerchantDetails());
			item.setRemarks(r.getRemarks());
			item.setCreateBy(r.getCreateBy());
			item.setCreateTime(r.getCreateTime());
			item.setUpdateBy(r.getUpdateBy());
			item.setUpdateTime(r.getUpdateTime());
			return item;
		}).collect(Collectors.toList());
	}

	@Override
	public MerchantInfoEvent getMerchantInfo() {
		MerchantInfoEvent merchantInfoEvent = this.baseMapper.tongJito();
		if (ObjectUtil.isNull(merchantInfoEvent)){
			merchantInfoEvent = new MerchantInfoEvent();
		}
		TaxInfoDto taxInfoDto = srsbBulletinTaxInformationMapper.tongji1();
		if (ObjectUtil.isNotNull(taxInfoDto)){
			merchantInfoEvent.setSale(taxInfoDto.getSale());
			merchantInfoEvent.setTaxAmount(taxInfoDto.getTaxAmount());
		}
		return merchantInfoEvent;
	}

	@Override
	public List<MerchantDataEvent> getMerchantOperation() {
		List<MerchantDataEvent> merchantOperation = this.baseMapper.getMerchantOperation();
		return merchantOperation;
	}
}
