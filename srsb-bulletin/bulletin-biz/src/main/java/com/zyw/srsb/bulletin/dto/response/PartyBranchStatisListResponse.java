package com.zyw.srsb.bulletin.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class PartyBranchStatisListResponse {
	private List<PartyBranchStatisResponse> partyBranchStatisList;
}
