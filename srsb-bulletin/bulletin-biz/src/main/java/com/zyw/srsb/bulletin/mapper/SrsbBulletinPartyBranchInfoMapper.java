package com.zyw.srsb.bulletin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyw.srsb.bulletin.entity.SrsbBulletinPartyBranchInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 党支部管理
 *
 * @author 朱耀威
 * @date 2022-04-09 20:28:21
 */
@Mapper
public interface SrsbBulletinPartyBranchInfoMapper extends BaseMapper<SrsbBulletinPartyBranchInfo> {

}
