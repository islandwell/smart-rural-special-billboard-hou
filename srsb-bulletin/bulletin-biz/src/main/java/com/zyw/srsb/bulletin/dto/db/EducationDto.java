package com.zyw.srsb.bulletin.dto.db;

import lombok.Data;

/**
 * @author 朱耀威
 */
@Data
public class EducationDto {
	private String education;
	private String gender;
	private Integer no;
	private String sort;
}
