package com.zyw.srsb.bulletin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyw.srsb.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 农作物
 *
 * @author 朱耀威
 * @date 2022-05-04 16:59:30
 */
@Data
@TableName("srsb_bulletin_crop")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "农作物")
public class SrsbBulletinCrop extends BaseEntity {

    /**
     * id
     */
    @TableId
    @ApiModelProperty(value="id")
    private Integer id;

    /**
     * 作物名称
     */
    @ApiModelProperty(value="作物名称")
    private String name;

    /**
     * 状态值
     */
    @ApiModelProperty(value="状态值")
    private Integer status;


}
