package com.zyw.srsb.bulletin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.bulletin.entity.SrsbBulletinIndustryNews;
import com.zyw.srsb.bulletin.service.SrsbBulletinIndustryNewsService;
import com.zyw.srsb.common.security.annotation.Inner;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 产业新闻
 *
 * @author 朱耀威
 * @date 2022-05-06 14:26:54
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletinindustrynews" )
@Api(value = "srsbbulletinindustrynews", tags = "产业新闻管理")
public class SrsbBulletinIndustryNewsController {

    private final  SrsbBulletinIndustryNewsService srsbBulletinIndustryNewsService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param srsbBulletinIndustryNews 产业新闻
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinindustrynews_get')" )
    public R getSrsbBulletinIndustryNewsPage(Page page, SrsbBulletinIndustryNews srsbBulletinIndustryNews) {
        return R.ok(srsbBulletinIndustryNewsService.page(page, Wrappers.query(srsbBulletinIndustryNews)));
    }


    /**
     * 通过id查询产业新闻
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinindustrynews_get')" )
    public R getById(@PathVariable("id" ) Integer id) {
        return R.ok(srsbBulletinIndustryNewsService.getById(id));
    }

    /**
     * 新增产业新闻
     * @param srsbBulletinIndustryNews 产业新闻
     * @return R
     */
    @ApiOperation(value = "新增产业新闻", notes = "新增产业新闻")
    @SysLog("新增产业新闻" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinindustrynews_add')" )
    public R save(@RequestBody SrsbBulletinIndustryNews srsbBulletinIndustryNews) {
        return R.ok(srsbBulletinIndustryNewsService.save(srsbBulletinIndustryNews));
    }

    /**
     * 修改产业新闻
     * @param srsbBulletinIndustryNews 产业新闻
     * @return R
     */
    @ApiOperation(value = "修改产业新闻", notes = "修改产业新闻")
    @SysLog("修改产业新闻" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinindustrynews_edit')" )
    public R updateById(@RequestBody SrsbBulletinIndustryNews srsbBulletinIndustryNews) {
        return R.ok(srsbBulletinIndustryNewsService.updateById(srsbBulletinIndustryNews));
    }

    /**
     * 通过id删除产业新闻
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除产业新闻", notes = "通过id删除产业新闻")
    @SysLog("通过id删除产业新闻" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinindustrynews_del')" )
    public R removeById(@PathVariable Integer id) {
        return R.ok(srsbBulletinIndustryNewsService.removeById(id));
    }

	/**
	 * 获取前num条产业新闻
	 * @param num
	 * @return
	 */
	@PostMapping("getNews/{num}")
	@ApiOperation(value = "获取前num条产业新闻", notes = "获取前num条产业新闻")
	@Inner(value = false)
	public R<List<SrsbBulletinIndustryNews>> getNews(@PathVariable Integer num){
		return R.ok(srsbBulletinIndustryNewsService.getNews(num));
	}
}
