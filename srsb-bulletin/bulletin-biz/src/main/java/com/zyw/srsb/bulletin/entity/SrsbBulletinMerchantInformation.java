package com.zyw.srsb.bulletin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyw.srsb.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 商户信息
 *
 * @author 朱耀威
 * @date 2022-02-13 13:48:20
 */
@Data
@TableName("srsb_bulletin_merchant_information")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "商户信息")
public class SrsbBulletinMerchantInformation extends BaseEntity {

    /**
     * 主键id
     */
    @TableId
    @ApiModelProperty(value="主键id")
    private Long id;

    /**
     * 经营类型
     */
    @ApiModelProperty(value="经营类型")
    private String businessType;

    /**
     * 编号
     */
    @ApiModelProperty(value="编号")
    private String code;

    /**
     * 名称
     */
    @ApiModelProperty(value="名称")
    private String name;

    /**
     * 地址
     */
    @ApiModelProperty(value="地址")
    private String address;

	/**
	 * 坐标点
	 */
	@ApiModelProperty(value="坐标点")
	private String point;

    /**
     * 联系人
     */
    @ApiModelProperty(value="联系人")
    private String contacts;

    /**
     * 经营时间
     */
    @ApiModelProperty(value="经营时间")
    private LocalDateTime operatingTime;

    /**
     * 经营范围
     */
    @ApiModelProperty(value="经营范围")
    private LocalDateTime businessNature;

    /**
     * 简介
     */
    @ApiModelProperty(value="简介")
    private String introduction;

    /**
     * 商户详情
     */
    @ApiModelProperty(value="商户详情")
    private String merchantDetails;

    /**
     * 备注
     */
    @ApiModelProperty(value="备注")
    private String remarks;


}
