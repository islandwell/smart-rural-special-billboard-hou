package com.zyw.srsb.bulletin.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.bulletin.entity.SrsbBulletinSmartCountrysideNews;
import com.zyw.srsb.bulletin.service.SrsbBulletinSmartCountrysideNewsService;
import com.zyw.srsb.common.security.annotation.Inner;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 智慧农村信息
 *
 * @author 朱耀威
 * @date 2022-05-08 02:05:39
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletinsmartcountrysidenews" )
@Api(value = "srsbbulletinsmartcountrysidenews", tags = "智慧农村信息管理")
public class SrsbBulletinSmartCountrysideNewsController {

    private final  SrsbBulletinSmartCountrysideNewsService srsbBulletinSmartCountrysideNewsService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param srsbBulletinSmartCountrysideNews 智慧农村信息
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinsmartcountrysidenews_get')" )
    public R getSrsbBulletinSmartCountrysideNewsPage(Page page, SrsbBulletinSmartCountrysideNews srsbBulletinSmartCountrysideNews) {
        return R.ok(srsbBulletinSmartCountrysideNewsService.page(page, Wrappers.query(srsbBulletinSmartCountrysideNews)));
    }


    /**
     * 通过id查询智慧农村信息
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinsmartcountrysidenews_get')" )
    public R getById(@PathVariable("id" ) Integer id) {
        return R.ok(srsbBulletinSmartCountrysideNewsService.getById(id));
    }

    /**
     * 新增智慧农村信息
     * @param srsbBulletinSmartCountrysideNews 智慧农村信息
     * @return R
     */
    @ApiOperation(value = "新增智慧农村信息", notes = "新增智慧农村信息")
    @SysLog("新增智慧农村信息" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinsmartcountrysidenews_add')" )
    public R save(@RequestBody SrsbBulletinSmartCountrysideNews srsbBulletinSmartCountrysideNews) {
        return R.ok(srsbBulletinSmartCountrysideNewsService.save(srsbBulletinSmartCountrysideNews));
    }

    /**
     * 修改智慧农村信息
     * @param srsbBulletinSmartCountrysideNews 智慧农村信息
     * @return R
     */
    @ApiOperation(value = "修改智慧农村信息", notes = "修改智慧农村信息")
    @SysLog("修改智慧农村信息" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinsmartcountrysidenews_edit')" )
    public R updateById(@RequestBody SrsbBulletinSmartCountrysideNews srsbBulletinSmartCountrysideNews) {
        return R.ok(srsbBulletinSmartCountrysideNewsService.updateById(srsbBulletinSmartCountrysideNews));
    }

    /**
     * 通过id删除智慧农村信息
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除智慧农村信息", notes = "通过id删除智慧农村信息")
    @SysLog("通过id删除智慧农村信息" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinsmartcountrysidenews_del')" )
    public R removeById(@PathVariable Integer id) {
        return R.ok(srsbBulletinSmartCountrysideNewsService.removeById(id));
    }

	@Inner(value = false)
	@PostMapping("/getNews/{num}")
	public R<List<SrsbBulletinSmartCountrysideNews>> getNews(@PathVariable Integer num){
		LambdaQueryWrapper<SrsbBulletinSmartCountrysideNews> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.orderByDesc(SrsbBulletinSmartCountrysideNews::getCreateTime).last("limit "+num.intValue());
		List<SrsbBulletinSmartCountrysideNews> list = srsbBulletinSmartCountrysideNewsService.list(queryWrapper);
		if (CollectionUtil.isNotEmpty(list)){
			return R.ok(list);
		}
		return R.ok();

	}


}
