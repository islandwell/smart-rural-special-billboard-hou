package com.zyw.srsb.bulletin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyw.srsb.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 粮食年产量表
 *
 * @author 朱耀威
 * @date 2022-04-04 17:00:19
 */
@Data
@TableName("srsb_bulletin_annual_grain_output")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "粮食年产量表")
public class SrsbBulletinAnnualGrainOutput extends BaseEntity {

    /**
     * id
     */
    @TableId
    @ApiModelProperty(value="id")
    private Long id;

    /**
     * 年份
     */
    @ApiModelProperty(value="年份")
    private String particularYear;

    /**
     * 全年粮食播种面积
     */
    @ApiModelProperty(value="全年粮食播种面积")
    private Double annualGrainSowingArea;

    /**
     * 单产值
     */
    @ApiModelProperty(value="单产值")
    private Double unitYieldValue;

    /**
     * 总产值
     */
    @ApiModelProperty(value="总产值")
    private Double totalOutputValue;

    /**
     * 夏粮亩产
     */
    @ApiModelProperty(value="夏粮亩产")
    private Double summerGrainYieldPerMu;

    /**
     * 夏粮总产
     */
    @ApiModelProperty(value="夏粮总产")
    private Double summerGrainTotalOutput;

    /**
     * 小麦亩产
     */
    @ApiModelProperty(value="小麦亩产")
    private Double wheatYieldPerMu;

    /**
     * 秋粮亩产
     */
    @ApiModelProperty(value="秋粮亩产")
    private Double autumnGrainYieldErMu;

    /**
     * 秋粮总产
     */
    @ApiModelProperty(value="秋粮总产")
    private Double totalAutumnGrainYield;

    /**
     * 水稻亩产
     */
    @ApiModelProperty(value="水稻亩产")
    private Double riceYieldPerMu;

    /**
     * 水稻总产
     */
    @ApiModelProperty(value="水稻总产")
    private Double totalRiceYield;


}
