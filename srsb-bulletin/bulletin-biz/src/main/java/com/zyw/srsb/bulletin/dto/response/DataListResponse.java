package com.zyw.srsb.bulletin.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class DataListResponse {
	List<TableResponse> list;
}
