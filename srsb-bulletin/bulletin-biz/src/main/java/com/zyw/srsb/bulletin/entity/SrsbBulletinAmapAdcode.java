package com.zyw.srsb.bulletin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyw.srsb.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 地理编码表
 *
 * @author 朱耀威
 * @date 2022-04-04 23:13:33
 */
@Data
@TableName("srsb_bulletin_amap_adcode")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "地理编码表")
public class SrsbBulletinAmapAdcode extends BaseEntity {

    /**
     * id
     */
    @TableId
    @ApiModelProperty(value="id")
    private Long id;

    /**
     * 名称
     */
    @ApiModelProperty(value="名称")
    private String name;

    /**
     * ad编码
     */
    @ApiModelProperty(value="ad编码")
    private String adcode;

    /**
     * 城市编码
     */
    @ApiModelProperty(value="城市编码")
    private String citycode;


}
