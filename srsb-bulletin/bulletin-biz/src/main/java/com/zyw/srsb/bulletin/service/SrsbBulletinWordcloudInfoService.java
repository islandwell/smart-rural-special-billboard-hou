package com.zyw.srsb.bulletin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.srsb.bulletin.entity.SrsbBulletinWordcloudInfo;

/**
 * 智慧农村词云表
 *
 * @author 朱耀威
 * @date 2022-05-07 11:58:54
 */
public interface SrsbBulletinWordcloudInfoService extends IService<SrsbBulletinWordcloudInfo> {

}
