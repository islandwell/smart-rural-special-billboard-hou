package com.zyw.srsb.bulletin.vo;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * 商户信息
 * @author 朱耀威
 * @date 2022/3/19
 */
@Data
@ColumnWidth(30)
public class SrsbBulletinMerchantInformationVo {

	/**
	 * 经营类型
	 */
	@ApiModelProperty(value="经营类型")
	@ExcelProperty("经营类型")
	@NotBlank
	private String businessType;

	/**
	 * 编号
	 */
	@ApiModelProperty(value="编号")
	@ExcelProperty("编号")
	private String code;

	/**
	 * 名称
	 */
	@ApiModelProperty(value="名称")
	@ExcelProperty("名称")
	private String name;

	/**
	 * 地址
	 */
	@ApiModelProperty(value="地址")
	@ExcelProperty("地址")
	private String address;

	/**
	 * 坐标点(百度)
	 */
	@ApiModelProperty(value="坐标点(百度)")
	@ExcelProperty("坐标点(百度)")
	private String point;

	/**
	 * 联系人
	 */
	@ApiModelProperty(value="联系人")
	@ExcelProperty("联系人")
	private String contacts;

	/**
	 * 经营时间
	 */
	@ApiModelProperty(value="经营时间")
	@ExcelProperty("经营时间")
	private String operatingTime;

	/**
	 * 经营范围
	 */
	@ApiModelProperty(value="经营范围")
	@ExcelProperty("经营范围")
	private LocalDateTime businessNature;

	/**
	 * 简介
	 */
	@ApiModelProperty(value="简介")
	@ExcelProperty("简介")
	private String introduction;

	/**
	 * 商户详情
	 */
	@ApiModelProperty(value="商户详情")
	@ExcelProperty("商户详情")
	private String merchantDetails;

	/**
	 * 备注
	 */
	@ApiModelProperty(value="备注")
	@ExcelProperty("备注")
	private String remarks;
}
