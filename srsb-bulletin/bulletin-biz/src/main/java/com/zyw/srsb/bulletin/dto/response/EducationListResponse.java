package com.zyw.srsb.bulletin.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author 朱耀威
 */
@Data
public class EducationListResponse {
	/**
	 * 教育列表
	 */
	@ApiModelProperty(value="教育列表")
	private List<String> educationNameList;
	/**
	 * 男 对应的学历人数数组
	 */
	@ApiModelProperty(value="男 对应的学历人数数组")
	private List<Integer> maleNoList;
	/**
	 * 女 对应的学历人数数组
	 */
	@ApiModelProperty(value="女 对应的学历人数数组")
	private List<Integer> femaleList;
}
