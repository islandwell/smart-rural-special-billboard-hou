package com.zyw.srsb.bulletin.dto.response;

import lombok.Data;

@Data
public class PartyBranchStatisResponse {
	private String partyOrganization;
	private Integer sum;
}
