package com.zyw.srsb.bulletin.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AllPartyBranchResponse {
	/**
	 * id
	 */
	@ApiModelProperty(value="id")
	private Integer id;

	/**
	 * 名称
	 */
	@ApiModelProperty(value="名称")
	private String name;

	/**
	 * 地址
	 */
	@ApiModelProperty(value="地址")
	private String address;

	/**
	 * 经度
	 */
	@ApiModelProperty(value="经度")
	private String longitude;

	/**
	 * 纬度
	 */
	@ApiModelProperty(value="纬度")
	private String latitude;

	/**
	 * 坐标系
	 */
	@ApiModelProperty(value="坐标系")
	private String pointSystem;

	/**
	 * 党支部介绍
	 */
	@ApiModelProperty(value="党支部介绍")
	private String context;

	/**
	 * 成立时间
	 */
	@ApiModelProperty(value="成立时间")
	private LocalDateTime establishmentTime;

	/**
	 * 状态
	 */
	@ApiModelProperty(value="状态")
	private Integer status;
}
