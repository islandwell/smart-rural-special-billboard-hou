package com.zyw.srsb.bulletin.dto.event;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

/**
 * 外链event
 *
 * @author 朱耀威
 * @date 2022-01-23 14:53:40
 */
@Data
public class LinkEvent {
	/**
	 * 看板id
	 */
	@ApiModelProperty(value="看板id",required = true)
	private Long boardId;

	/**
	 * 入口url
	 */
	@ApiModelProperty(value="入口url",required = true)
	@NotBlank(message = "入口url不能为空")
	private String entryUrl;

	/**
	 * 链接状态 0-失效 1-生效
	 */
	@ApiModelProperty(value="链接状态 0-失效 1-生效")
	private Integer status;

	/**
	 * 失效时间
	 */
	@ApiModelProperty(value="失效时间")
	private LocalDateTime endTime;
}
