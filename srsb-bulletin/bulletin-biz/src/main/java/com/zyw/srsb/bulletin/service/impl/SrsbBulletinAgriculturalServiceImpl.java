package com.zyw.srsb.bulletin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyw.srsb.bulletin.entity.SrsbBulletinAgricultural;
import com.zyw.srsb.bulletin.mapper.SrsbBulletinAgriculturalMapper;
import com.zyw.srsb.bulletin.service.SrsbBulletinAgriculturalService;
import org.springframework.stereotype.Service;

/**
 * 农业产值表
 *
 * @author 朱耀威
 * @date 2022-04-04 17:23:31
 */
@Service
public class SrsbBulletinAgriculturalServiceImpl extends ServiceImpl<SrsbBulletinAgriculturalMapper, SrsbBulletinAgricultural> implements SrsbBulletinAgriculturalService {

}
