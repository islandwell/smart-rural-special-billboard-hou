package com.zyw.srsb.bulletin.dto.event;

import lombok.Data;

import java.math.BigInteger;

@Data
public class MerchantDataEvent {
	private String name;
	private BigInteger value;
}
