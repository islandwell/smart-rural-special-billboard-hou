package com.zyw.srsb.bulletin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyw.srsb.bulletin.dto.event.GetBaseInfoEvent;
import com.zyw.srsb.bulletin.entity.SrsbBulletinHousehold;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * 农户
 *
 * @author 朱耀威
 * @date 2022-05-04 16:29:27
 */
@Mapper
public interface SrsbBulletinHouseholdMapper extends BaseMapper<SrsbBulletinHousehold> {
	@Select("SELECT count(1) as num,sum(area) as area,sum(sales) as sale FROM `srsb_bulletin_household`")
	GetBaseInfoEvent tongji1();
}
