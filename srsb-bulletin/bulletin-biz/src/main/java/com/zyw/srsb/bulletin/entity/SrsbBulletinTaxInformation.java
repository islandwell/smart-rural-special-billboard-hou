package com.zyw.srsb.bulletin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyw.srsb.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.math.BigDecimal;

/**
 * 纳税信息表
 *
 * @author 朱耀威
 * @date 2022-05-04 18:24:20
 */
@Data
@TableName("srsb_bulletin_tax_information")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "纳税信息表")
public class SrsbBulletinTaxInformation extends BaseEntity {

    /**
     * 主键id
     */
    @TableId
    @ApiModelProperty(value="主键id")
    private Long id;

    /**
     * 年份
     */
    @ApiModelProperty(value="年份")
    private String year;

    /**
     * 商铺名称
     */
    @ApiModelProperty(value="商铺名称")
    private String name;

    /**
     * 纳税额（万元）
     */
    @ApiModelProperty(value="纳税额（万元）")
    private BigDecimal taxAmount;

    /**
     * 销售额（万元）
     */
    @ApiModelProperty(value="销售额（万元）")
    private BigDecimal saleVolume;

    /**
     * 商户id
     */
    @ApiModelProperty(value="商户id")
    private Long merchantId;


}
