package com.zyw.srsb.bulletin.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyw.srsb.bulletin.dto.event.GetBaseInfoEvent;
import com.zyw.srsb.bulletin.entity.SrsbBulletinCrop;
import com.zyw.srsb.bulletin.entity.SrsbBulletinHousehold;
import com.zyw.srsb.bulletin.mapper.SrsbBulletinCropMapper;
import com.zyw.srsb.bulletin.mapper.SrsbBulletinHouseholdMapper;
import com.zyw.srsb.bulletin.service.SrsbBulletinHouseholdService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

/**
 * 农户
 *
 * @author 朱耀威
 * @date 2022-05-04 16:29:27
 */
@Service
public class SrsbBulletinHouseholdServiceImpl extends ServiceImpl<SrsbBulletinHouseholdMapper, SrsbBulletinHousehold> implements SrsbBulletinHouseholdService {
	@Autowired
	private SrsbBulletinCropMapper srsbBulletinCropMapper;

	@Override
	public GetBaseInfoEvent getBaseInfo() {
		GetBaseInfoEvent baseInfoEvent = this.baseMapper.tongji1();
		if (ObjectUtil.isNull(baseInfoEvent)) {
			baseInfoEvent = new GetBaseInfoEvent();
		}
		// 查询作物种类
		BigInteger bigInteger = srsbBulletinCropMapper.tongji1();
		if (ObjectUtil.isNotNull(bigInteger)) {
			baseInfoEvent.setTypeNum(bigInteger);
		}
		return baseInfoEvent;
	}
}
