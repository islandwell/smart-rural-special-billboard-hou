package com.zyw.srsb.bulletin.dto.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * 天气event
 * @author 朱耀威
 */
@Data
@NoArgsConstructor
public class WetherResponse {
	/**
	 * 当前时间
	 */
	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss",timezone = "GMT+8")
	private LocalDateTime nowTime;

	/**
	 * 天气描述
	 */
	private String text;

	/**
	 * 温度
	 */
	private String temp;

	/**
	 * 风力等级
	 */
	private String windScale;

	/**
	 * 风向
	 */
	private String windDir;

}
