package com.zyw.srsb.bulletin.mapper;

import com.zyw.srsb.bulletin.dto.response.PartyBasicResponse;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 智慧农村专题看板
 */
@Mapper
public interface SmartPartyBuildingMapper {
	PartyBasicResponse queryPartyBasic(@Param(value = "villageName")String villageName);
}
