package com.zyw.srsb.bulletin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyw.srsb.bulletin.entity.SrsbBulletinAnnualGrainOutput;
import com.zyw.srsb.bulletin.mapper.SrsbBulletinAnnualGrainOutputMapper;
import com.zyw.srsb.bulletin.service.SrsbBulletinAnnualGrainOutputService;
import org.springframework.stereotype.Service;

/**
 * 粮食年产量表
 *
 * @author 朱耀威
 * @date 2022-04-04 17:00:19
 */
@Service
public class SrsbBulletinAnnualGrainOutputServiceImpl extends ServiceImpl<SrsbBulletinAnnualGrainOutputMapper, SrsbBulletinAnnualGrainOutput> implements SrsbBulletinAnnualGrainOutputService {

}
