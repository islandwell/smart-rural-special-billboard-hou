package com.zyw.srsb.bulletin.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyw.srsb.bulletin.entity.SrsbBulletinWordcloudInfo;
import com.zyw.srsb.bulletin.mapper.SrsbBulletinWordcloudInfoMapper;
import com.zyw.srsb.bulletin.service.SrsbBulletinWordcloudInfoService;
import org.springframework.stereotype.Service;

/**
 * 智慧农村词云表
 *
 * @author 朱耀威
 * @date 2022-05-07 11:58:54
 */
@Service
public class SrsbBulletinWordcloudInfoServiceImpl extends ServiceImpl<SrsbBulletinWordcloudInfoMapper, SrsbBulletinWordcloudInfo> implements SrsbBulletinWordcloudInfoService {

}
