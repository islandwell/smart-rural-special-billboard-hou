package com.zyw.srsb.bulletin.dto.response;

import lombok.Data;

/**
 * @author 朱耀威
 */
@Data
public class TableResponse {
	private String name;
	private Integer value;
}
