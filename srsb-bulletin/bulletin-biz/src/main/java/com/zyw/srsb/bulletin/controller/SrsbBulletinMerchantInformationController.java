package com.zyw.srsb.bulletin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.plugin.excel.annotation.RequestExcel;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import com.zyw.srsb.bulletin.dto.event.MerchantDataEvent;
import com.zyw.srsb.bulletin.dto.event.MerchantInfoEvent;
import com.zyw.srsb.bulletin.entity.SrsbBulletinMerchantInformation;
import com.zyw.srsb.bulletin.service.SrsbBulletinMerchantInformationService;
import com.zyw.srsb.bulletin.vo.SrsbBulletinMerchantInformationVo;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.common.security.annotation.Inner;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 商户信息
 *
 * @author 朱耀威
 * @date 2022-02-13 13:48:20
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletinmerchantinformation" )
@Api(value = "srsbbulletinmerchantinformation", tags = "商户信息管理")
public class SrsbBulletinMerchantInformationController {

    private final  SrsbBulletinMerchantInformationService srsbBulletinMerchantInformationService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param srsbBulletinMerchantInformation 商户信息
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinmerchantinformation_get')" )
    public R getSrsbBulletinMerchantInformationPage(Page page, SrsbBulletinMerchantInformation srsbBulletinMerchantInformation) {
        return R.ok(srsbBulletinMerchantInformationService.page(page, Wrappers.query(srsbBulletinMerchantInformation)));
    }


    /**
     * 通过id查询商户信息
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinmerchantinformation_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(srsbBulletinMerchantInformationService.getById(id));
    }

    /**
     * 新增商户信息
     * @param srsbBulletinMerchantInformation 商户信息
     * @return R
     */
    @ApiOperation(value = "新增商户信息", notes = "新增商户信息")
    @SysLog("新增商户信息" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinmerchantinformation_add')" )
    public R save(@RequestBody SrsbBulletinMerchantInformation srsbBulletinMerchantInformation) {
        return R.ok(srsbBulletinMerchantInformationService.save(srsbBulletinMerchantInformation));
    }

    /**
     * 修改商户信息
     * @param srsbBulletinMerchantInformation 商户信息
     * @return R
     */
    @ApiOperation(value = "修改商户信息", notes = "修改商户信息")
    @SysLog("修改商户信息" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinmerchantinformation_edit')" )
    public R updateById(@RequestBody SrsbBulletinMerchantInformation srsbBulletinMerchantInformation) {
        return R.ok(srsbBulletinMerchantInformationService.updateById(srsbBulletinMerchantInformation));
    }

    /**
     * 通过id删除商户信息
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除商户信息", notes = "通过id删除商户信息")
    @SysLog("通过id删除商户信息" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinmerchantinformation_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(srsbBulletinMerchantInformationService.removeById(id));
    }

	/**
	 * 导入商户信息
	 * @param excelVOList
	 * @param bindingResult
	 * @return
	 */
	@PostMapping("/import")
	@SysLog("导入商户信息" )
	@PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinmerchantinformation_import')")
	public R importSrsbBulletinMerchantInformation(@RequestExcel List<SrsbBulletinMerchantInformationVo> excelVOList, BindingResult bindingResult) {
		srsbBulletinMerchantInformationService.importSrsbBulletinMerchantInformation(excelVOList,bindingResult);
		return R.ok();
	}


	/**
	 * 导出商户信息
	 * @return
	 */
	@ResponseExcel
	@GetMapping("/export")
	@SysLog("导出商户信息" )
	@PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinmerchantinformation_export')")
	public List export() {
		return srsbBulletinMerchantInformationService.export();
	}

	/**
	 * 看板获取商户信息
	 * @return
	 */
	@PostMapping("/getMerchantInfo")
	@ApiOperation(value = "看板获取商户信息",notes = "看板获取商户信息")
	@Inner(value = false)
	public R<MerchantInfoEvent> getMerchantInfo(){
		return R.ok(srsbBulletinMerchantInformationService.getMerchantInfo());
	}

	/**
	 * 商户经营种类柱形图
	 * @return
	 */
	@PostMapping("/getMerchantOperation")
	@Inner(value = false)
	public R<List<MerchantDataEvent>> getMerchantOperation(){
		return R.ok(srsbBulletinMerchantInformationService.getMerchantOperation());
	}


}
