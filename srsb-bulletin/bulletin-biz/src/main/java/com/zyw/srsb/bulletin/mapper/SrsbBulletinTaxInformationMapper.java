package com.zyw.srsb.bulletin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyw.srsb.bulletin.dto.db.TaxInfoDto;
import com.zyw.srsb.bulletin.dto.event.MerchantDataEvent;
import com.zyw.srsb.bulletin.entity.SrsbBulletinTaxInformation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 纳税信息表
 *
 * @author 朱耀威
 * @date 2022-05-04 18:24:20
 */
@Mapper
public interface SrsbBulletinTaxInformationMapper extends BaseMapper<SrsbBulletinTaxInformation> {
	@Select("select sum(tax_amount) as taxAmount,sum(sale_volume)  as sale FROM srsb_bulletin_tax_information")
	TaxInfoDto tongji1();
}
