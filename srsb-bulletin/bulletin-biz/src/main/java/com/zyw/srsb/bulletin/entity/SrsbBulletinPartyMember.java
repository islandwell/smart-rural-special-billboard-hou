/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the srsb4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.zyw.srsb.bulletin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyw.srsb.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 党员信息表
 *
 * @author 朱耀威
 * @date 2022-01-23 15:58:17
 */
@Data
@TableName("srsb_bulletin_party_member")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "党员信息表")
public class SrsbBulletinPartyMember extends BaseEntity {

    /**
     * 主键id
     */
    @TableId
    @ApiModelProperty(value="主键id")
    private Long id;

    /**
     * 所属党组织
     */
    @ApiModelProperty(value="所属党组织")
    private String partyOrganization;

    /**
     * 党组织code
     */
    @ApiModelProperty(value="党组织code")
    private String code;

    /**
     * 姓名
     */
    @ApiModelProperty(value="姓名")
    private String fullName;

    /**
     * 身份证号码
     */
    @ApiModelProperty(value="身份证号码")
    private String cardNo;

    /**
     * 手机号码
     */
    @ApiModelProperty(value="手机号码")
    private String phoneNo;

    /**
     * 交纳党费
     */
    @ApiModelProperty(value="交纳党费")
    private Long payPartyDues;

    /**
     * 性别 0-男 1-女
     */
    @ApiModelProperty(value="性别 0-男 1-女")
    private String gender;

    /**
     * 年龄
     */
    @ApiModelProperty(value="年龄")
    private Integer age;

    /**
     * 党龄
     */
    @ApiModelProperty(value="党龄")
    private Integer partyAge;

    /**
     * 学历
     */
    @ApiModelProperty(value="学历")
    private String education;

    /**
     * 参加工作时间
     */
    @ApiModelProperty(value="参加工作时间")
    private LocalDateTime workingTime;

    /**
     * 名族
     */
    @ApiModelProperty(value="名族")
    private String nation;

    /**
     * 出生年月
     */
    @ApiModelProperty(value="出生年月")
    private LocalDateTime birthday;

    /**
     * 入党年月
     */
    @ApiModelProperty(value="入党年月")
    private LocalDateTime joinDate;

    /**
     * 专业
     */
    @ApiModelProperty(value="专业")
    private String major;

    /**
     * 任职单位
     */
    @ApiModelProperty(value="任职单位")
    private String employer;

    /**
     * 最后交费时间
     */
    @ApiModelProperty(value="最后交费时间")
    private LocalDateTime lastPaymentDate;


}
