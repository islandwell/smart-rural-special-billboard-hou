package com.zyw.srsb.bulletin.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.bulletin.entity.SrsbBulletinWordcloudInfo;
import com.zyw.srsb.bulletin.service.SrsbBulletinWordcloudInfoService;
import com.zyw.srsb.common.security.annotation.Inner;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 智慧农村词云表
 *
 * @author 朱耀威
 * @date 2022-05-07 11:58:54
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletinwordcloudinfo" )
@Api(value = "srsbbulletinwordcloudinfo", tags = "智慧农村词云表管理")
public class SrsbBulletinWordcloudInfoController {

    private final  SrsbBulletinWordcloudInfoService srsbBulletinWordcloudInfoService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param srsbBulletinWordcloudInfo 智慧农村词云表
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinwordcloudinfo_get')" )
    public R getSrsbBulletinWordcloudInfoPage(Page page, SrsbBulletinWordcloudInfo srsbBulletinWordcloudInfo) {
        return R.ok(srsbBulletinWordcloudInfoService.page(page, Wrappers.query(srsbBulletinWordcloudInfo)));
    }


    /**
     * 通过id查询智慧农村词云表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinwordcloudinfo_get')" )
    public R getById(@PathVariable("id" ) Integer id) {
        return R.ok(srsbBulletinWordcloudInfoService.getById(id));
    }

    /**
     * 新增智慧农村词云表
     * @param srsbBulletinWordcloudInfo 智慧农村词云表
     * @return R
     */
    @ApiOperation(value = "新增智慧农村词云表", notes = "新增智慧农村词云表")
    @SysLog("新增智慧农村词云表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinwordcloudinfo_add')" )
    public R save(@RequestBody SrsbBulletinWordcloudInfo srsbBulletinWordcloudInfo) {
        return R.ok(srsbBulletinWordcloudInfoService.save(srsbBulletinWordcloudInfo));
    }

    /**
     * 修改智慧农村词云表
     * @param srsbBulletinWordcloudInfo 智慧农村词云表
     * @return R
     */
    @ApiOperation(value = "修改智慧农村词云表", notes = "修改智慧农村词云表")
    @SysLog("修改智慧农村词云表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinwordcloudinfo_edit')" )
    public R updateById(@RequestBody SrsbBulletinWordcloudInfo srsbBulletinWordcloudInfo) {
        return R.ok(srsbBulletinWordcloudInfoService.updateById(srsbBulletinWordcloudInfo));
    }

    /**
     * 通过id删除智慧农村词云表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除智慧农村词云表", notes = "通过id删除智慧农村词云表")
    @SysLog("通过id删除智慧农村词云表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinwordcloudinfo_del')" )
    public R removeById(@PathVariable Integer id) {
        return R.ok(srsbBulletinWordcloudInfoService.removeById(id));
    }

	/**
	 * 获取智慧农村词云数据
	 */
	@ApiOperation(value = "获取智慧农村词云数据", notes = "获取智慧农村词云数据")
	@PostMapping("/getWordCloudList")
	@Inner(value = false)
	public R<List<SrsbBulletinWordcloudInfo>> getWordCloudList(){
		return R.ok(srsbBulletinWordcloudInfoService.getBaseMapper().selectList(new QueryWrapper<>()));
	}


}
