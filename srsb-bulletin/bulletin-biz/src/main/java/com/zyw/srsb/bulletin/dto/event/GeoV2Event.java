package com.zyw.srsb.bulletin.dto.event;

import lombok.Data;

/**
 * @author 朱耀威
 */
@Data
public class GeoV2Event {
	private String city;
	private String address;
}
