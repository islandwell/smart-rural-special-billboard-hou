package com.zyw.srsb.bulletin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.bulletin.entity.SrsbBulletinTaxInformation;
import com.zyw.srsb.bulletin.service.SrsbBulletinTaxInformationService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 纳税信息表
 *
 * @author 朱耀威
 * @date 2022-05-04 18:24:20
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletintaxinformation" )
@Api(value = "srsbbulletintaxinformation", tags = "纳税信息表管理")
public class SrsbBulletinTaxInformationController {

    private final  SrsbBulletinTaxInformationService srsbBulletinTaxInformationService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param srsbBulletinTaxInformation 纳税信息表
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletintaxinformation_get')" )
    public R getSrsbBulletinTaxInformationPage(Page page, SrsbBulletinTaxInformation srsbBulletinTaxInformation) {
        return R.ok(srsbBulletinTaxInformationService.page(page, Wrappers.query(srsbBulletinTaxInformation)));
    }


    /**
     * 通过id查询纳税信息表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletintaxinformation_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(srsbBulletinTaxInformationService.getById(id));
    }

    /**
     * 新增纳税信息表
     * @param srsbBulletinTaxInformation 纳税信息表
     * @return R
     */
    @ApiOperation(value = "新增纳税信息表", notes = "新增纳税信息表")
    @SysLog("新增纳税信息表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletintaxinformation_add')" )
    public R save(@RequestBody SrsbBulletinTaxInformation srsbBulletinTaxInformation) {
        return R.ok(srsbBulletinTaxInformationService.save(srsbBulletinTaxInformation));
    }

    /**
     * 修改纳税信息表
     * @param srsbBulletinTaxInformation 纳税信息表
     * @return R
     */
    @ApiOperation(value = "修改纳税信息表", notes = "修改纳税信息表")
    @SysLog("修改纳税信息表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletintaxinformation_edit')" )
    public R updateById(@RequestBody SrsbBulletinTaxInformation srsbBulletinTaxInformation) {
        return R.ok(srsbBulletinTaxInformationService.updateById(srsbBulletinTaxInformation));
    }

    /**
     * 通过id删除纳税信息表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除纳税信息表", notes = "通过id删除纳税信息表")
    @SysLog("通过id删除纳税信息表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletintaxinformation_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(srsbBulletinTaxInformationService.removeById(id));
    }

}
