package com.zyw.srsb.bulletin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.srsb.bulletin.dto.event.MerchantDataEvent;
import com.zyw.srsb.bulletin.dto.event.MerchantInfoEvent;
import com.zyw.srsb.bulletin.dto.response.SrsbBulletinMerchantInformationResponse;
import com.zyw.srsb.bulletin.entity.SrsbBulletinMerchantInformation;
import com.zyw.srsb.bulletin.vo.SrsbBulletinMerchantInformationVo;
import org.springframework.validation.BindingResult;

import java.util.List;

/**
 * 商户信息
 *
 * @author 朱耀威
 * @date 2022-02-13 13:48:20
 */
public interface SrsbBulletinMerchantInformationService extends IService<SrsbBulletinMerchantInformation> {

	/**
	 * 导入商户信息
	 * @param excelVOList
	 * @param bindingResult
	 */
	void importSrsbBulletinMerchantInformation(List<SrsbBulletinMerchantInformationVo> excelVOList, BindingResult bindingResult);

	/**
	 * 导出商户信息
	 * @return
	 */
	List<SrsbBulletinMerchantInformationResponse> export();

	/**
	 * 看板获取商户信息
	 * @return
	 */
    MerchantInfoEvent getMerchantInfo();


	/**
	 * 看板获取柱形图
	 * @return
	 */
	List<MerchantDataEvent> getMerchantOperation();
}
