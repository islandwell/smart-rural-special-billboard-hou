package com.zyw.srsb.bulletin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pig4cloud.plugin.excel.annotation.RequestExcel;
import com.pig4cloud.plugin.excel.annotation.ResponseExcel;
import com.zyw.srsb.admin.api.dto.UserDTO;
import com.zyw.srsb.bulletin.dto.response.*;
import com.zyw.srsb.bulletin.vo.SrsbBulletinPartyMemberVo;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.bulletin.entity.SrsbBulletinPartyMember;
import com.zyw.srsb.bulletin.service.SrsbBulletinPartyMemberService;
import com.zyw.srsb.common.security.annotation.Inner;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 党员信息表
 *
 * @author 朱耀威
 * @date 2022-01-23 15:58:17
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletinpartymember" )
@Api(value = "srsbbulletinpartymember", tags = "党员信息表管理")
public class SrsbBulletinPartyMemberController {

    private final  SrsbBulletinPartyMemberService srsbBulletinPartyMemberService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param srsbBulletinPartyMember 党员信息表
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinpartymember_get')" )
    public R getSrsbBulletinPartyMemberPage(Page page, SrsbBulletinPartyMember srsbBulletinPartyMember) {
        return R.ok(srsbBulletinPartyMemberService.page(page, Wrappers.query(srsbBulletinPartyMember)));
    }


    /**
     * 通过id查询党员信息表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinpartymember_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(srsbBulletinPartyMemberService.getById(id));
    }

    /**
     * 新增党员信息表
     * @param srsbBulletinPartyMember 党员信息表
     * @return R
     */
    @ApiOperation(value = "新增党员信息表", notes = "新增党员信息表")
    @SysLog("新增党员信息表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinpartymember_add')" )
    public R save(@RequestBody SrsbBulletinPartyMember srsbBulletinPartyMember) {
        return R.ok(srsbBulletinPartyMemberService.save(srsbBulletinPartyMember));
    }

    /**
     * 修改党员信息表
     * @param srsbBulletinPartyMember 党员信息表
     * @return R
     */
    @ApiOperation(value = "修改党员信息表", notes = "修改党员信息表")
    @SysLog("修改党员信息表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinpartymember_edit')" )
    public R updateById(@RequestBody SrsbBulletinPartyMember srsbBulletinPartyMember) {
        return R.ok(srsbBulletinPartyMemberService.updateById(srsbBulletinPartyMember));
    }

    /**
     * 通过id删除党员信息表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除党员信息表", notes = "通过id删除党员信息表")
    @SysLog("通过id删除党员信息表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinpartymember_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(srsbBulletinPartyMemberService.removeById(id));
    }

	/**
	 * 党支部统计
	 */
	@ApiOperation(value = "党支部统计", notes = "党支部统计")
	@Inner(value = false)
	@PostMapping("/queryPartyBranchStatistics")
	public R<PartyBranchStatisListResponse> queryPartyBranchStatistics(){
		return R.ok(srsbBulletinPartyMemberService.queryPartyBranchStatistics());
	}

	/**
	 * 年份统计人数统计
	 */
	@ApiOperation(value = "年份统计人数统计", notes = "年份统计人数统计")
	@Inner(value = false)
	@PostMapping("/annualPopulationStatistics")
	public R<DataListResponse> annualPopulationStatistics(){
		return R.ok(srsbBulletinPartyMemberService.annualPopulationStatistics());
	}

	@ApiOperation(value = "党员学历统计", notes = "党员学历统计")
	@Inner(value = false)
	@PostMapping("/partyEducationalStatistics")
	public R<EducationListResponse> partyEducationalStatistics(){
		return R.ok(srsbBulletinPartyMemberService.partyEducationalStatistics());
	}

	@ApiOperation(value = "性别比例", notes = "性别比例")
	@Inner(value = false)
	@PostMapping("/querySexRatio")
	public R<SexRatioResponse> querySexRatio(){
		return R.ok(srsbBulletinPartyMemberService.querySexRatio());
	}

	/**
	 * 交纳党费记录
	 */
	@ApiOperation(value = "交纳党费记录",notes = "交纳党费记录")
	@Inner(value = false)
	@PostMapping("/queryPartyFeePaymentRecord")
	public R<PartyFeePaymentRecordListResponse> queryPartyFeePaymentRecord(){
		return R.ok(srsbBulletinPartyMemberService.queryPartyFeePaymentRecord());
	}
	/**
	 * 党员发展折线图
	 */
	@ApiOperation(value = "党员发展折线图",notes = "党员发展折线图")
	@Inner(value = false)
	@PostMapping("/queryPartyMemberDevelopmentResponse")
	public R<PartyMemberDevelopmentResponse> queryPartyMemberDevelopmentResponse(){
		return R.ok(srsbBulletinPartyMemberService.queryPartyMemberDevelopmentResponse());
	}

	/**
	 * 导入党员信息
	 * @param excelVOList
	 * @param bindingResult
	 * @return
	 */
	@PostMapping("/import")
	@SysLog("导入党员信息" )
	@PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinpartymember_import')")
	public R importSrsbBulletinPartyMember(@RequestExcel List<SrsbBulletinPartyMemberVo> excelVOList, BindingResult bindingResult) {
		srsbBulletinPartyMemberService.importSrsbBulletinPartyMember(excelVOList,bindingResult);
		return R.ok();
	}

	/**
	 * 导出导入党员信息excel 表格
	 * @return
	 */
	@ResponseExcel
	@SysLog("导出导入党员信息excel 表格" )
	@GetMapping("/export")
	@PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinpartymember_export')")
	public List export() {
		return srsbBulletinPartyMemberService.listSrsbBulletinPartyMember();
	}
}
