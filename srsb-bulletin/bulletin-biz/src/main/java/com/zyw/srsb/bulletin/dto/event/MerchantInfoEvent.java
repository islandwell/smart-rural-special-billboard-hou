package com.zyw.srsb.bulletin.dto.event;

import lombok.Data;

import java.math.BigInteger;

/**
 * 商户信息event
 *
 * @author 朱耀威
 * @date 2022-01-23 14:53:40
 */
@Data
public class MerchantInfoEvent {
	public MerchantInfoEvent() {
		this.num = 0;
		this.typeNum = 0;
		this.sale = new BigInteger("0");
		this.taxAmount = new BigInteger("0");
	}

	/**
	 * 商户数量
	 */
	private Integer num;
	/**
	 * 经营种类
	 */
	private Integer typeNum;
	/**
	 * 销售额
	 */
	private BigInteger sale;
	/**
	 * 纳税额
	 */
	private BigInteger taxAmount;


}
