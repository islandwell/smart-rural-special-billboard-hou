package com.zyw.srsb.bulletin.service;

import com.zyw.srsb.bulletin.dto.response.WetherResponse;
import com.zyw.srsb.datacollect.dto.event.WeatherEvent;

/**
 * @author 朱耀威
 */
public interface SrsbWeatherService {
	WetherResponse queryWeaterInformation(WeatherEvent event);
}
