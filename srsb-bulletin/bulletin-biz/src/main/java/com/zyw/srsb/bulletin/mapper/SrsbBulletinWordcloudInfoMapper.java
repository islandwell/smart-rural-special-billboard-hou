package com.zyw.srsb.bulletin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyw.srsb.bulletin.entity.SrsbBulletinWordcloudInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * 智慧农村词云表
 *
 * @author 朱耀威
 * @date 2022-05-07 11:58:54
 */
@Mapper
public interface SrsbBulletinWordcloudInfoMapper extends BaseMapper<SrsbBulletinWordcloudInfo> {

}
