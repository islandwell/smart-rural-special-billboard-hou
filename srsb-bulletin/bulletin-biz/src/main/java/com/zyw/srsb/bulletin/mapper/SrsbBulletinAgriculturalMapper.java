package com.zyw.srsb.bulletin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyw.srsb.bulletin.entity.SrsbBulletinAgricultural;
import org.apache.ibatis.annotations.Mapper;

/**
 * 农业产值表
 *
 * @author 朱耀威
 * @date 2022-04-04 17:23:31
 */
@Mapper
public interface SrsbBulletinAgriculturalMapper extends BaseMapper<SrsbBulletinAgricultural> {

}
