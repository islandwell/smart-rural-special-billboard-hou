package com.zyw.srsb.bulletin.dto.response;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 党员信息
 * @author 朱耀威
 * @date 2022/3/19
 */
@Data
@ColumnWidth(30)
public class ExportSrsbBulletinPartyMemberResponse implements Serializable {
	private static final long serialVersionUID = 3791280844838288057L;
	/**
	 * 主键id
	 */
	@ApiModelProperty(value="主键id")
	@ExcelProperty("主键id")
	private Long id;

	/**
	 * 所属党组织
	 */
	@ApiModelProperty(value="所属党组织")
	@ExcelProperty("所属党组织")
	private String partyOrganization;

	/**
	 * 党组织code
	 */
	@ApiModelProperty(value="党组织code")
	@ExcelProperty("党组织code")
	private String code;

	/**
	 * 姓名
	 */
	@ApiModelProperty(value="姓名")
	@ExcelProperty("姓名")
	private String fullName;

	/**
	 * 身份证号码
	 */
	@ApiModelProperty(value="身份证号码")
	@ExcelProperty("身份证号码")
	private String cardNo;

	/**
	 * 手机号码
	 */
	@ApiModelProperty(value="手机号码")
	@ExcelProperty("手机号码")
	private String phoneNo;

	/**
	 * 交纳党费
	 */
	@ApiModelProperty(value="交纳党费")
	@ExcelProperty("交纳党费")
	private Long payPartyDues;

	/**
	 * 性别 0-男 1-女
	 */
	@ApiModelProperty(value="性别 0-男 1-女")
	@ExcelProperty("性别")
	private String gender;

	/**
	 * 年龄
	 */
	@ApiModelProperty(value="年龄")
	@ExcelProperty("年龄")
	private Integer age;

	/**
	 * 党龄
	 */
	@ApiModelProperty(value="党龄")
	@ExcelProperty("党龄")
	private Integer partyAge;

	/**
	 * 学历
	 */
	@ApiModelProperty(value="学历")
	@ExcelProperty("学历")
	private String education;

	/**
	 * 参加工作时间
	 */
	@ApiModelProperty(value="参加工作时间")
	@ExcelProperty("参加工作时间")
	private LocalDateTime workingTime;

	/**
	 * 名族
	 */
	@ApiModelProperty(value="民族")
	@ExcelProperty("民族")
	private String nation;

	/**
	 * 出生年月
	 */
	@ApiModelProperty(value="出生年月")
	@ExcelProperty("出生年月")
	@DateTimeFormat(pattern = "y/M/d")
	private LocalDateTime birthday;

	/**
	 * 入党年月
	 */
	@ApiModelProperty(value="入党年月")
	@ExcelProperty("入党年月")
	@DateTimeFormat(pattern = "y/M/d")
	private LocalDateTime joinDate;

	/**
	 * 专业
	 */
	@ApiModelProperty(value="专业")
	@ExcelProperty("专业")
	private String major;

	/**
	 * 任职单位
	 */
	@ApiModelProperty(value="任职单位")
	@ExcelProperty("任职单位")
	private String employer;

	/**
	 * 最后交费时间
	 */
	@ApiModelProperty(value="最后交费时间")
	@DateTimeFormat(pattern = "y/M/d")
	@ExcelProperty("最后交费时间")
	private LocalDateTime lastPaymentDate;

	/**
	 * 创建者
	 */
	@TableField(fill = FieldFill.INSERT)
	@ExcelProperty("创建者")
	private String createBy;

	/**
	 * 创建时间
	 */
	@TableField(fill = FieldFill.INSERT)
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	@ExcelProperty("创建时间")
	private LocalDateTime createTime;

	/**
	 * 更新者
	 */
	@ExcelProperty("更新者")
	private String updateBy;

	/**
	 * 更新时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	@ExcelProperty("更新时间")
	private LocalDateTime updateTime;

}
