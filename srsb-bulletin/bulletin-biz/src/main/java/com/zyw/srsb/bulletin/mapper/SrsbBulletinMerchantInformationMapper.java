package com.zyw.srsb.bulletin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyw.srsb.bulletin.dto.event.MerchantDataEvent;
import com.zyw.srsb.bulletin.dto.event.MerchantInfoEvent;
import com.zyw.srsb.bulletin.entity.SrsbBulletinMerchantInformation;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 商户信息
 *
 * @author 朱耀威
 * @date 2022-02-13 13:48:20
 */
@Mapper
public interface SrsbBulletinMerchantInformationMapper extends BaseMapper<SrsbBulletinMerchantInformation> {
	@Select("select count(1) as num,count(DISTINCT business_type) as typeNum from srsb_bulletin_merchant_information")
	MerchantInfoEvent tongJito();

	@Select("SELECT business_type as name,count(1) as value FROM `srsb_bulletin_merchant_information` GROUP BY business_type")
	List<MerchantDataEvent> getMerchantOperation();
}
