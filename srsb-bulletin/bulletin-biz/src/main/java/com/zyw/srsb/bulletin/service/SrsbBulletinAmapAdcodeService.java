package com.zyw.srsb.bulletin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.srsb.bulletin.entity.SrsbBulletinAmapAdcode;

/**
 * 地理编码表
 *
 * @author 朱耀威
 * @date 2022-04-04 23:13:33
 */
public interface SrsbBulletinAmapAdcodeService extends IService<SrsbBulletinAmapAdcode> {

}
