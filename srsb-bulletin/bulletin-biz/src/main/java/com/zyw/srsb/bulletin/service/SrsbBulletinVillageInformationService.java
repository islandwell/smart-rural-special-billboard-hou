package com.zyw.srsb.bulletin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.srsb.bulletin.entity.SrsbBulletinVillageInformation;

/**
 * 村庄信息表
 *
 * @author 朱耀威
 * @date 2022-03-27 14:51:01
 */
public interface SrsbBulletinVillageInformationService extends IService<SrsbBulletinVillageInformation> {

	/**
	 * 获取村庄信息
	 * @param name
	 * @return
	 */
	SrsbBulletinVillageInformation findSrsbBulletinVillageInformation(String name);
}
