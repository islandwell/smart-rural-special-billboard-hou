package com.zyw.srsb.bulletin.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SexRatioResponse {
	@ApiModelProperty(value="男性比例")
	private Integer maleRatio;
	@ApiModelProperty(value="女性比例")
	private Integer femaleRatio;
}
