package com.zyw.srsb.bulletin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.srsb.bulletin.dto.response.AllPartyBranchResponse;
import com.zyw.srsb.bulletin.entity.SrsbBulletinPartyBranchInfo;

import java.util.List;

/**
 * 党支部管理
 *
 * @author 朱耀威
 * @date 2022-04-09 20:28:21
 */
public interface SrsbBulletinPartyBranchInfoService extends IService<SrsbBulletinPartyBranchInfo> {

	/**
	 * 获取所有的党支部信息
	 * @return
	 */
	List<AllPartyBranchResponse> getAllPartyBranchList();
}
