package com.zyw.srsb.bulletin.dto.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author 朱耀威
 * 党建基础情况
 */
@Data
@ApiModel(value = "党建基础情况基础类")
public class PartyBasicResponse {
	@ApiModelProperty(value="村庄名称")
	private String villageName;
	@ApiModelProperty(value="基础组织个数")
	private Long branchesNo;
	@ApiModelProperty(value="党员总数")
	private Long totalPartyNo;
	@ApiModelProperty(value="交纳总党费")
	private Long totalPartyPay;
	@ApiModelProperty(value="平均年龄")
	private Long averageAge;
	@ApiModelProperty(value="平均党龄")
	private Integer	averagePartyAge;
}
