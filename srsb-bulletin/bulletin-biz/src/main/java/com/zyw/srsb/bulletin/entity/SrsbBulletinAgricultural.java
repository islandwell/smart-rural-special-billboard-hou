package com.zyw.srsb.bulletin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyw.srsb.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 农业产值表
 *
 * @author 朱耀威
 * @date 2022-04-04 17:23:31
 */
@Data
@TableName("srsb_bulletin_agricultural")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "农业产值表")
public class SrsbBulletinAgricultural extends BaseEntity {

    /**
     * 主键id
     */
    @TableId
    @ApiModelProperty(value="主键id")
    private Long id;

    /**
     * 年份
     */
    @ApiModelProperty(value="年份")
    private String particulaYear;

    /**
     * 农业产值
     */
    @ApiModelProperty(value="农业产值")
    private Double agriculturalOutputValue;

    /**
     * 林业产值
     */
    @ApiModelProperty(value="林业产值")
    private Double forestryOutputValue;

    /**
     * 牧业产值
     */
    @ApiModelProperty(value="牧业产值")
    private Double animalHusbandryOutputValue;

    /**
     * 渔业产值
     */
    @ApiModelProperty(value="渔业产值")
    private Double fisheryOutputValue;

    /**
     * 农村服务业产值
     */
    @ApiModelProperty(value="农村服务业产值")
    private Double serviceOutputValue;


}
