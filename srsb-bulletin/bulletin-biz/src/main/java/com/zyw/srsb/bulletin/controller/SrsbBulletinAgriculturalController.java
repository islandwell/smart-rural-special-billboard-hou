package com.zyw.srsb.bulletin.controller;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.bulletin.entity.SrsbBulletinAgricultural;
import com.zyw.srsb.bulletin.service.SrsbBulletinAgriculturalService;
import com.zyw.srsb.common.security.annotation.Inner;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 农业产值表
 *
 * @author 朱耀威
 * @date 2022-04-04 17:23:31
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletinagricultural" )
@Api(value = "srsbbulletinagricultural", tags = "农业产值表管理")
public class SrsbBulletinAgriculturalController {

    private final  SrsbBulletinAgriculturalService srsbBulletinAgriculturalService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param srsbBulletinAgricultural 农业产值表
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinagricultural_get')" )
    public R getSrsbBulletinAgriculturalPage(Page page, SrsbBulletinAgricultural srsbBulletinAgricultural) {
        return R.ok(srsbBulletinAgriculturalService.page(page, Wrappers.query(srsbBulletinAgricultural)));
    }


    /**
     * 通过id查询农业产值表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinagricultural_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(srsbBulletinAgriculturalService.getById(id));
    }

    /**
     * 新增农业产值表
     * @param srsbBulletinAgricultural 农业产值表
     * @return R
     */
    @ApiOperation(value = "新增农业产值表", notes = "新增农业产值表")
    @SysLog("新增农业产值表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinagricultural_add')" )
    public R save(@RequestBody SrsbBulletinAgricultural srsbBulletinAgricultural) {
        return R.ok(srsbBulletinAgriculturalService.save(srsbBulletinAgricultural));
    }

    /**
     * 修改农业产值表
     * @param srsbBulletinAgricultural 农业产值表
     * @return R
     */
    @ApiOperation(value = "修改农业产值表", notes = "修改农业产值表")
    @SysLog("修改农业产值表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinagricultural_edit')" )
    public R updateById(@RequestBody SrsbBulletinAgricultural srsbBulletinAgricultural) {
        return R.ok(srsbBulletinAgriculturalService.updateById(srsbBulletinAgricultural));
    }

    /**
     * 通过id删除农业产值表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除农业产值表", notes = "通过id删除农业产值表")
    @SysLog("通过id删除农业产值表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinagricultural_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(srsbBulletinAgriculturalService.removeById(id));
    }

	@PostMapping("/getBulletinAgricultural/{year}")
	@Inner(value = false)
	@ApiOperation(value = "显示农业产值", notes = "显示农业产值")
	public R<SrsbBulletinAgricultural> getBulletinAgricultural(@PathVariable String year){
		LambdaQueryWrapper<SrsbBulletinAgricultural> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.eq(SrsbBulletinAgricultural::getParticulaYear,year).last("limit 1");
		List<SrsbBulletinAgricultural> list = srsbBulletinAgriculturalService.list(queryWrapper);
		if (CollectionUtil.isNotEmpty(list)){
			return R.ok(list.get(0));
		}
		return R.ok();
	}

}
