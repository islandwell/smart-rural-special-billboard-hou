package com.zyw.srsb.bulletin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyw.srsb.bulletin.dto.response.AllPartyBranchResponse;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.bulletin.entity.SrsbBulletinPartyBranchInfo;
import com.zyw.srsb.bulletin.service.SrsbBulletinPartyBranchInfoService;
import com.zyw.srsb.common.security.annotation.Inner;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 党支部管理
 *
 * @author 朱耀威
 * @date 2022-04-09 20:28:21
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletinpartybranchinfo" )
@Api(value = "srsbbulletinpartybranchinfo", tags = "党支部管理管理")
public class SrsbBulletinPartyBranchInfoController {

    private final  SrsbBulletinPartyBranchInfoService srsbBulletinPartyBranchInfoService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param srsbBulletinPartyBranchInfo 党支部管理
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinpartybranchinfo_get')" )
    public R getSrsbBulletinPartyBranchInfoPage(Page page, SrsbBulletinPartyBranchInfo srsbBulletinPartyBranchInfo) {
        return R.ok(srsbBulletinPartyBranchInfoService.page(page, Wrappers.query(srsbBulletinPartyBranchInfo)));
    }


    /**
     * 通过id查询党支部管理
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinpartybranchinfo_get')" )
    public R getById(@PathVariable("id" ) Integer id) {
        return R.ok(srsbBulletinPartyBranchInfoService.getById(id));
    }

    /**
     * 新增党支部管理
     * @param srsbBulletinPartyBranchInfo 党支部管理
     * @return R
     */
    @ApiOperation(value = "新增党支部管理", notes = "新增党支部管理")
    @SysLog("新增党支部管理" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinpartybranchinfo_add')" )
    public R save(@RequestBody SrsbBulletinPartyBranchInfo srsbBulletinPartyBranchInfo) {
        return R.ok(srsbBulletinPartyBranchInfoService.save(srsbBulletinPartyBranchInfo));
    }

    /**
     * 修改党支部管理
     * @param srsbBulletinPartyBranchInfo 党支部管理
     * @return R
     */
    @ApiOperation(value = "修改党支部管理", notes = "修改党支部管理")
    @SysLog("修改党支部管理" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinpartybranchinfo_edit')" )
    public R updateById(@RequestBody SrsbBulletinPartyBranchInfo srsbBulletinPartyBranchInfo) {
        return R.ok(srsbBulletinPartyBranchInfoService.updateById(srsbBulletinPartyBranchInfo));
    }

    /**
     * 通过id删除党支部管理
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除党支部管理", notes = "通过id删除党支部管理")
    @SysLog("通过id删除党支部管理" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinpartybranchinfo_del')" )
    public R removeById(@PathVariable Integer id) {
        return R.ok(srsbBulletinPartyBranchInfoService.removeById(id));
    }

	/**
	 * 获取所有党支部的数据
	 */
	@Inner(value = false)
	@ApiOperation(value = "获取党支部数据", notes = "获取党支部数据")
	@PostMapping("/getAllPartyBranchList")
	public R<List<AllPartyBranchResponse>> getAllPartyBranchList(){
		return R.ok(srsbBulletinPartyBranchInfoService.getAllPartyBranchList());
	}


}
