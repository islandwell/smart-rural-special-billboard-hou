package com.zyw.srsb.bulletin.dto.db;

import lombok.Data;

@Data
public class YearNumDto {
	private String year;
	private Integer num;
}
