package com.zyw.srsb.bulletin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.bulletin.entity.SrsbBulletinVillageInformation;
import com.zyw.srsb.bulletin.service.SrsbBulletinVillageInformationService;
import com.zyw.srsb.common.security.annotation.Inner;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 村庄信息表
 *
 * @author 朱耀威
 * @date 2022-03-27 14:51:01
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletinvillageinformation" )
@Api(value = "srsbbulletinvillageinformation", tags = "村庄信息表管理")
public class SrsbBulletinVillageInformationController {

    private final  SrsbBulletinVillageInformationService srsbBulletinVillageInformationService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param srsbBulletinVillageInformation 村庄信息表
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinvillageinformation_get')" )
    public R getSrsbBulletinVillageInformationPage(Page page, SrsbBulletinVillageInformation srsbBulletinVillageInformation) {
        return R.ok(srsbBulletinVillageInformationService.page(page, Wrappers.query(srsbBulletinVillageInformation)));
    }


    /**
     * 通过id查询村庄信息表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinvillageinformation_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(srsbBulletinVillageInformationService.getById(id));
    }

    /**
     * 新增村庄信息表
     * @param srsbBulletinVillageInformation 村庄信息表
     * @return R
     */
    @ApiOperation(value = "新增村庄信息表", notes = "新增村庄信息表")
    @SysLog("新增村庄信息表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinvillageinformation_add')" )
    public R save(@RequestBody SrsbBulletinVillageInformation srsbBulletinVillageInformation) {
        return R.ok(srsbBulletinVillageInformationService.save(srsbBulletinVillageInformation));
    }

    /**
     * 修改村庄信息表
     * @param srsbBulletinVillageInformation 村庄信息表
     * @return R
     */
    @ApiOperation(value = "修改村庄信息表", notes = "修改村庄信息表")
    @SysLog("修改村庄信息表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinvillageinformation_edit')" )
    public R updateById(@RequestBody SrsbBulletinVillageInformation srsbBulletinVillageInformation) {
        return R.ok(srsbBulletinVillageInformationService.updateById(srsbBulletinVillageInformation));
    }

    /**
     * 通过id删除村庄信息表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除村庄信息表", notes = "通过id删除村庄信息表")
    @SysLog("通过id删除村庄信息表")
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinvillageinformation_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(srsbBulletinVillageInformationService.removeById(id));
    }

	/**
	 * 获取村庄信息
	 * @param name
	 * @return
	 */
	@ApiOperation(value = "获取村庄信息", notes = "获取村庄信息")
	@PostMapping("/findSrsbBulletinVillageInformation/{name}")
	@Inner(value = false)
	public R<SrsbBulletinVillageInformation> findSrsbBulletinVillageInformation(@PathVariable String name){
		return R.ok(srsbBulletinVillageInformationService.findSrsbBulletinVillageInformation(name));
	}

}
