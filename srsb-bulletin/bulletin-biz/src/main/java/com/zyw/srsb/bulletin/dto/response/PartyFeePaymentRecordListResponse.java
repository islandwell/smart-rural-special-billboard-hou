package com.zyw.srsb.bulletin.dto.response;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 查查询党费交纳记录
 */
@Data
public class PartyFeePaymentRecordListResponse {
	private List<PartyFeePaymentRecord> partyFeePaymentRecordList;

	@Data
	public static class PartyFeePaymentRecord{
		@ApiModelProperty(value="姓名")
		private String fullName;
		@ApiModelProperty(value="手机号")
		private String phoneNo;
		@ApiModelProperty(value="付款金额")
		private BigInteger payPartyDues;
		@ApiModelProperty(value="最后交纳时间")
		@DateTimeFormat(pattern = "yyyy-MM-dd")
		private LocalDateTime lastPaymentDate;

		public String getPhoneNo() {
			return phoneNo.replaceAll("(\\d{3})\\d{4}(\\d{4})","$1****$2");
		}
	}

}
