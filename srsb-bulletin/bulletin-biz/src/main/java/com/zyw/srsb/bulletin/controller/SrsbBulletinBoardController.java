package com.zyw.srsb.bulletin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.bulletin.entity.SrsbBulletinBoard;
import com.zyw.srsb.bulletin.service.SrsbBulletinBoardService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 看板
 *
 * @author 朱耀威
 * @date 2022-01-23 13:56:27
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletinboard" )
@Api(value = "srsbbulletinboard", tags = "看板管理")
public class SrsbBulletinBoardController {

    private final  SrsbBulletinBoardService srsbBulletinBoardService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param srsbBulletinBoard 看板
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinboard_get')" )
    public R getSrsbBulletinBoardPage(Page page, SrsbBulletinBoard srsbBulletinBoard) {
        return R.ok(srsbBulletinBoardService.page(page, Wrappers.query(srsbBulletinBoard)));
    }


    /**
     * 通过id查询看板
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinboard_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(srsbBulletinBoardService.getById(id));
    }

    /**
     * 新增看板
     * @param srsbBulletinBoard 看板
     * @return R
     */
    @ApiOperation(value = "新增看板", notes = "新增看板")
    @SysLog("新增看板" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinboard_add')" )
    public R save(@RequestBody SrsbBulletinBoard srsbBulletinBoard) {
        return R.ok(srsbBulletinBoardService.save(srsbBulletinBoard));
    }

    /**
     * 修改看板
     * @param srsbBulletinBoard 看板
     * @return R
     */
    @ApiOperation(value = "修改看板", notes = "修改看板")
    @SysLog("修改看板" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinboard_edit')" )
    public R updateById(@RequestBody SrsbBulletinBoard srsbBulletinBoard) {
        return R.ok(srsbBulletinBoardService.updateById(srsbBulletinBoard));
    }

    /**
     * 通过id删除看板
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除看板", notes = "通过id删除看板")
    @SysLog("通过id删除看板" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinboard_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(srsbBulletinBoardService.removeById(id));
    }

}
