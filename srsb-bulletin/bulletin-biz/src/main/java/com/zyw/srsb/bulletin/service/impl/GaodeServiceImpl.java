package com.zyw.srsb.bulletin.service.impl;

import com.zyw.srsb.bulletin.dto.event.GeoV2Event;
import com.zyw.srsb.bulletin.dto.response.GeoResponse;
import com.zyw.srsb.bulletin.service.GaodeService;
import com.zyw.srsb.common.core.constant.SecurityConstants;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.datacollect.dto.GeoDto;
import com.zyw.srsb.datacollect.dto.event.GeoEvent;
import com.zyw.srsb.datacollect.feign.GaodeApiFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 高德服务
 */
@Service
public class GaodeServiceImpl implements GaodeService {
	@Autowired
	GaodeApiFeignService gaodeApiFeignService;

	@Override
	public GeoResponse getGeoLocation(GeoV2Event event) {
		GeoResponse geoResponse = new GeoResponse();
		try {
			GeoEvent geoEvent = new GeoEvent();
			geoEvent.setCity(event.getCity());
			geoEvent.setAddress(event.getAddress());
			R<GeoDto> geoDtoR = gaodeApiFeignService.geoCode(geoEvent, SecurityConstants.FROM_IN);
			String[] split = geoDtoR.getData().getGeocodes().get(0).getLocation().split(",");
			geoResponse.setLongitude(split[0]);
			geoResponse.setLatitude(split[1]);
		} catch (Exception e){}
		geoResponse.setPointSystem("WGS-84");
		return geoResponse;
	}
}
