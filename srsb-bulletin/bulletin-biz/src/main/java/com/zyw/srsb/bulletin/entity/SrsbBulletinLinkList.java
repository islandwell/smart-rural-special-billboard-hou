/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the srsb4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.zyw.srsb.bulletin.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyw.srsb.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * 看板外链
 *
 * @author 朱耀威
 * @date 2022-01-23 14:53:40
 */
@Data
@TableName("srsb_bulletin_link_list")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "看板外链")
public class SrsbBulletinLinkList extends BaseEntity {

    /**
     * 主键id
     */
    @TableId
    @ApiModelProperty(value="主键id")
    private Long id;

    /**
     * 看板id
     */
    @ApiModelProperty(value="看板id")
    private Long boardId;

    /**
     * 入口url
     */
    @ApiModelProperty(value="入口url")
    private String entryUrl;

    /**
     * 链接密文
     */
    @ApiModelProperty(value="链接密文")
    private String ciphertext;

    /**
     * 链接状态 0-失效 1-生效
     */
    @ApiModelProperty(value="链接状态 0-失效 1-生效")
    private Integer status;

    /**
     * 失效时间
     */
    @ApiModelProperty(value="失效时间")
    private LocalDateTime endTime;


}
