package com.zyw.srsb.bulletin.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.system.SystemUtil;
import com.zyw.srsb.bulletin.dto.response.WetherResponse;
import com.zyw.srsb.bulletin.service.SrsbWeatherService;
import com.zyw.srsb.common.core.constant.SecurityConstants;
import com.zyw.srsb.common.core.exception.ValidateCodeException;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.datacollect.dto.WeatherDto;
import com.zyw.srsb.datacollect.dto.event.WeatherEvent;
import com.zyw.srsb.datacollect.feign.WeatherFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;

@Service
public class SrsbWeatherServiceImpl implements SrsbWeatherService {
	@Autowired
	WeatherFeignService weatherFeignService;
	/**
	 * 查询天气详情
	 * @param event
	 * @return
	 */
	@Override
	public WetherResponse queryWeaterInformation(WeatherEvent event) {
		R<WeatherDto> nowWeather = weatherFeignService.getNowWeather(event, SecurityConstants.FROM_IN);

		if (!ObjectUtil.isAllNotEmpty(nowWeather,nowWeather.getData())){
			throw new ValidateCodeException("获取天气失败!");
		}
		WeatherDto data = nowWeather.getData();

		WetherResponse response = new WetherResponse();
		response.setTemp(data.getNow().getTemp());
		response.setWindDir(data.getNow().getWindDir());
		response.setWindScale(data.getNow().getWindScale());
		response.setText(data.getNow().getText());
		response.setNowTime(LocalDateTime.now());
		return response;
	}
}
