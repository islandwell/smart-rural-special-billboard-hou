package com.zyw.srsb.bulletin.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyw.srsb.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 农户
 *
 * @author 朱耀威
 * @date 2022-05-04 16:29:27
 */
@Data
@TableName("srsb_bulletin_household")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "农户")
public class SrsbBulletinHousehold extends BaseEntity {

    /**
     * 主键id
     */
    @TableId
    @ApiModelProperty(value="主键id")
    private Integer id;

    /**
     * 农户名称
     */
    @ApiModelProperty(value="农户名称")
    private String name;

    /**
     * 地址
     */
    @ApiModelProperty(value="地址")
    private String address;

    /**
     * 总种植面积(亩)
     */
    @ApiModelProperty(value="总种植面积(亩)")
    private Integer area;

    /**
     * 销量(元)
     */
    @ApiModelProperty(value="销量(元)")
    private String sales;


}
