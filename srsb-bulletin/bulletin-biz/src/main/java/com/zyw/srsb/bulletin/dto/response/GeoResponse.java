package com.zyw.srsb.bulletin.dto.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@ApiModel("GeoResponse")
@Data
public class GeoResponse {
	/**
	 * 经度
	 */
	@ApiModelProperty(value="经度")
	private String longitude;

	/**
	 * 纬度
	 */
	@ApiModelProperty(value="纬度")
	private String latitude;

	/**
	 * 坐标系
	 */
	@ApiModelProperty(value="坐标系")
	private String pointSystem;
}
