package com.zyw.srsb.bulletin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zyw.srsb.bulletin.entity.SrsbBulletinIndustryNews;
import com.zyw.srsb.bulletin.mapper.SrsbBulletinIndustryNewsMapper;
import com.zyw.srsb.bulletin.service.SrsbBulletinIndustryNewsService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 产业新闻
 *
 * @author 朱耀威
 * @date 2022-05-06 14:26:54
 */
@Service
public class SrsbBulletinIndustryNewsServiceImpl extends ServiceImpl<SrsbBulletinIndustryNewsMapper, SrsbBulletinIndustryNews> implements SrsbBulletinIndustryNewsService {

	/**
	 * 获取前num条产业视频
	 * @param num
	 * @return
	 */
	@Override
	public List<SrsbBulletinIndustryNews> getNews(Integer num) {
		LambdaQueryWrapper<SrsbBulletinIndustryNews> queryWrapper = new LambdaQueryWrapper<>();
		queryWrapper.orderByDesc(SrsbBulletinIndustryNews::getCreateTime).last("limit "+num.intValue());
		return this.list(queryWrapper);
	}
}
