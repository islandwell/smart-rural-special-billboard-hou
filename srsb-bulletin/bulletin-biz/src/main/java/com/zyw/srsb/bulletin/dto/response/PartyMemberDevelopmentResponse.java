package com.zyw.srsb.bulletin.dto.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * @author 朱耀威
 */
@Data
public class PartyMemberDevelopmentResponse {
	@ApiModelProperty(value="年份列表(正序排序)")
	private List<String> yearList;

	@ApiModelProperty(value="年份入党人数列表")
	private List<Integer> yearNoList;

	@ApiModelProperty(value="年份入党总人数列表")
	private List<Integer> yearTotalNoList;
}
