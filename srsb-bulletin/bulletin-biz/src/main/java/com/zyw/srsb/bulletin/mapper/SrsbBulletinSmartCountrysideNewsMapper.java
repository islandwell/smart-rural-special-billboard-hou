package com.zyw.srsb.bulletin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyw.srsb.bulletin.entity.SrsbBulletinSmartCountrysideNews;
import org.apache.ibatis.annotations.Mapper;

/**
 * 智慧农村信息
 *
 * @author 朱耀威
 * @date 2022-05-08 02:05:39
 */
@Mapper
public interface SrsbBulletinSmartCountrysideNewsMapper extends BaseMapper<SrsbBulletinSmartCountrysideNews> {

}
