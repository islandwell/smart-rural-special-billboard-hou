package com.zyw.srsb.bulletin.service;

import com.zyw.srsb.bulletin.dto.event.GeoV2Event;
import com.zyw.srsb.bulletin.dto.response.GeoResponse;

/**
 * @author 朱耀威
 */
public interface GaodeService {
	/**
	 * 获取坐标
	 * @param event
	 * @return
	 */
	GeoResponse getGeoLocation(GeoV2Event event);
}
