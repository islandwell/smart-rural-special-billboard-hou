package com.zyw.srsb.bulletin.dto.event;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 是否鉴权event
 *
 * @author 朱耀威
 * @date 2022-03-26 14:53:40
 */
@Data
public class IsAuthEvent {
	/**
	 * 看板名称
	 */
	@ApiModelProperty(value="看板名称",required = false)
	private String name;

	@ApiModelProperty(value="token",required = true)
	@NotBlank(message = "token不能为空")
	private String token;

	@ApiModelProperty(value="url",required = true)
	@NotBlank(message = "访问的url不能为空")
	private String url;

}
