package com.zyw.srsb.bulletin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.srsb.bulletin.entity.SrsbBulletinAnnualGrainOutput;

/**
 * 粮食年产量表
 *
 * @author 朱耀威
 * @date 2022-04-04 17:00:19
 */
public interface SrsbBulletinAnnualGrainOutputService extends IService<SrsbBulletinAnnualGrainOutput> {

}
