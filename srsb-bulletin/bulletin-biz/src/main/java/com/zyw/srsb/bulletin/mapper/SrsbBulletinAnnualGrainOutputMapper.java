package com.zyw.srsb.bulletin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyw.srsb.bulletin.entity.SrsbBulletinAnnualGrainOutput;
import org.apache.ibatis.annotations.Mapper;

/**
 * 粮食年产量表
 *
 * @author 朱耀威
 * @date 2022-04-04 17:00:19
 */
@Mapper
public interface SrsbBulletinAnnualGrainOutputMapper extends BaseMapper<SrsbBulletinAnnualGrainOutput> {

}
