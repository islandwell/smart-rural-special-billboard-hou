package com.zyw.srsb.bulletin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.srsb.bulletin.entity.SrsbBulletinAgricultural;

/**
 * 农业产值表
 *
 * @author 朱耀威
 * @date 2022-04-04 17:23:31
 */
public interface SrsbBulletinAgriculturalService extends IService<SrsbBulletinAgricultural> {

}
