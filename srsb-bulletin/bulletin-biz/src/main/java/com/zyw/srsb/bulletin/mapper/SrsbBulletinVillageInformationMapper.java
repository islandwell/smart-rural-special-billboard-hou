package com.zyw.srsb.bulletin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyw.srsb.bulletin.entity.SrsbBulletinVillageInformation;
import org.apache.ibatis.annotations.Mapper;

/**
 * 村庄信息表
 *
 * @author 朱耀威
 * @date 2022-03-27 14:51:01
 */
@Mapper
public interface SrsbBulletinVillageInformationMapper extends BaseMapper<SrsbBulletinVillageInformation> {

}
