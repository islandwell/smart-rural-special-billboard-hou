package com.zyw.srsb.bulletin.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

/**
 * 世界转换工具
 */
public class TimeUtils {
	public static String T1 = "y/M/d";

	/**
	 * str转localdatetime
	 * @param str
	 * @param format
	 * @return
	 */
	public static LocalDateTime stringToLocalDateTime(String str, String format){
		LocalDateTime localDate = null;
		try {
			DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(format, Locale.CHINA);
			LocalDate localDateTime = LocalDate.parse(str,dateTimeFormatter);
			Date date = Date.from(localDateTime.atStartOfDay(ZoneOffset.ofHours(8)).toInstant());
			localDate = date.toInstant().atZone(ZoneOffset.ofHours(8)).toLocalDateTime();
		}catch (Exception e){
			System.out.println(e);
		}
		return localDate;
	}

	public static LocalDateTime stringToLocalDateTime(String str){
		return stringToLocalDateTime(str,T1);
	}

	/**
	 * LocalDateTime转String
	 * @param time
	 * @param format
	 * @return
	 */
	public static String localDateTimeToString(LocalDateTime time, String format){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		String format1 = null;
		try {
			format1 = time.format(formatter);
		} catch (Exception e){}
		return format;
	}

	/**
	 * LocalDateTime转String
	 * @param time
	 * @param format
	 * @return
	 */
	public static String localDateTimeToString(LocalDateTime time){
		return localDateTimeToString(time,T1);
	}
}
