package com.zyw.srsb.bulletin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.srsb.bulletin.dto.event.GetBaseInfoEvent;
import com.zyw.srsb.bulletin.entity.SrsbBulletinHousehold;

/**
 * 农户
 *
 * @author 朱耀威
 * @date 2022-05-04 16:29:27
 */
public interface SrsbBulletinHouseholdService extends IService<SrsbBulletinHousehold> {

	/**
	 * 看板获取基本情况信息
	 * @return
	 */
	GetBaseInfoEvent getBaseInfo();
}
