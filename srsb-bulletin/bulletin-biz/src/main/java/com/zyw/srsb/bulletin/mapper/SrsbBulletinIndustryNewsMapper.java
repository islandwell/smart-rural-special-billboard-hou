package com.zyw.srsb.bulletin.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zyw.srsb.bulletin.entity.SrsbBulletinIndustryNews;
import org.apache.ibatis.annotations.Mapper;

/**
 * 产业新闻
 *
 * @author 朱耀威
 * @date 2022-05-06 14:26:54
 */
@Mapper
public interface SrsbBulletinIndustryNewsMapper extends BaseMapper<SrsbBulletinIndustryNews> {

}
