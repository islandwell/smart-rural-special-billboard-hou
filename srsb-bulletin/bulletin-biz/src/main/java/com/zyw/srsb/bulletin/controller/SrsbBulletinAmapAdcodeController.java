package com.zyw.srsb.bulletin.controller;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.bulletin.entity.SrsbBulletinAmapAdcode;
import com.zyw.srsb.bulletin.service.SrsbBulletinAmapAdcodeService;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


/**
 * 地理编码表
 *
 * @author 朱耀威
 * @date 2022-04-04 23:13:33
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/srsbbulletinamapadcode" )
@Api(value = "srsbbulletinamapadcode", tags = "地理编码表管理")
public class SrsbBulletinAmapAdcodeController {

    private final  SrsbBulletinAmapAdcodeService srsbBulletinAmapAdcodeService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param srsbBulletinAmapAdcode 地理编码表
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinamapadcode_get')" )
    public R getSrsbBulletinAmapAdcodePage(Page page, SrsbBulletinAmapAdcode srsbBulletinAmapAdcode) {
        return R.ok(srsbBulletinAmapAdcodeService.page(page, Wrappers.query(srsbBulletinAmapAdcode)));
    }


    /**
     * 通过id查询地理编码表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinamapadcode_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(srsbBulletinAmapAdcodeService.getById(id));
    }

    /**
     * 新增地理编码表
     * @param srsbBulletinAmapAdcode 地理编码表
     * @return R
     */
    @ApiOperation(value = "新增地理编码表", notes = "新增地理编码表")
    @SysLog("新增地理编码表" )
    @PostMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinamapadcode_add')" )
    public R save(@RequestBody SrsbBulletinAmapAdcode srsbBulletinAmapAdcode) {
        return R.ok(srsbBulletinAmapAdcodeService.save(srsbBulletinAmapAdcode));
    }

    /**
     * 修改地理编码表
     * @param srsbBulletinAmapAdcode 地理编码表
     * @return R
     */
    @ApiOperation(value = "修改地理编码表", notes = "修改地理编码表")
    @SysLog("修改地理编码表" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinamapadcode_edit')" )
    public R updateById(@RequestBody SrsbBulletinAmapAdcode srsbBulletinAmapAdcode) {
        return R.ok(srsbBulletinAmapAdcodeService.updateById(srsbBulletinAmapAdcode));
    }

    /**
     * 通过id删除地理编码表
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除地理编码表", notes = "通过id删除地理编码表")
    @SysLog("通过id删除地理编码表" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('bulletin_srsbbulletinamapadcode_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(srsbBulletinAmapAdcodeService.removeById(id));
    }

}
