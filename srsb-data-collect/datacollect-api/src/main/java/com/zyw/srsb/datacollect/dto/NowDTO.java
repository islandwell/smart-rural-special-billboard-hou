package com.zyw.srsb.datacollect.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class NowDTO {
    @JSONField(name = "obsTime")
    private String obsTime;
    @JSONField(name = "temp")
    private String temp;
    @JSONField(name = "feelsLike")
    private String feelsLike;
    @JSONField(name = "icon")
    private String icon;
    @JSONField(name = "text")
    private String text;
    @JSONField(name = "wind360")
    private String wind360;
    @JSONField(name = "windDir")
    private String windDir;
    @JSONField(name = "windScale")
    private String windScale;
    @JSONField(name = "windSpeed")
    private String windSpeed;
    @JSONField(name = "humidity")
    private String humidity;
    @JSONField(name = "precip")
    private String precip;
    @JSONField(name = "pressure")
    private String pressure;
    @JSONField(name = "vis")
    private String vis;
    @JSONField(name = "cloud")
    private String cloud;
    @JSONField(name = "dew")
    private String dew;
}
