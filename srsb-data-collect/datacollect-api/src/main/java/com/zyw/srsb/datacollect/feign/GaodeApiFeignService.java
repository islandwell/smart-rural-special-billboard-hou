package com.zyw.srsb.datacollect.feign;

import com.zyw.srsb.common.core.constant.SecurityConstants;
import com.zyw.srsb.common.core.constant.ServiceNameConstants;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.datacollect.dto.GeoDto;
import com.zyw.srsb.datacollect.dto.event.GeoEvent;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @author 朱耀威
 */
@FeignClient(contextId = "GaodeApiService", value = ServiceNameConstants.DATA_COLLECT_SERVE)
public interface GaodeApiFeignService {
	/**
	 * 高德编码解析
	 *
	 * @return
	 */
	@PostMapping("/data-collect/v3/geocode/geo")
	R<GeoDto> geoCode(@RequestBody GeoEvent event, @RequestHeader(SecurityConstants.FROM) String from);
}
