package com.zyw.srsb.datacollect.dto.event;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 高德地理正向解析响应头
 * @author 朱耀威
 * @date 2022/4/9
 */
@Data
@NoArgsConstructor
@ApiModel(value = "正向解析响应头")
public class GeoEvent {
	@ApiModelProperty(value = "key",required = true)
	private String key;
	@ApiModelProperty(value = "城市",required = true)
	private String city;
	@ApiModelProperty(value = "地址",required = true)
	private String address;
}
