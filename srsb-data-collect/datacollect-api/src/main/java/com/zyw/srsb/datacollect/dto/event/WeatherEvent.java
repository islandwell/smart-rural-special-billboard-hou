package com.zyw.srsb.datacollect.dto.event;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class WeatherEvent {
	/**
     * location
	 */
	private String location;
	/**
     * key
	 */
	private String key;
	/**
     * baseUrl
	 */
	private String baseUrl;
}
