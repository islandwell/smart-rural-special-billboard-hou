package com.zyw.srsb.datacollect.feign;

import com.zyw.srsb.common.core.constant.SecurityConstants;
import com.zyw.srsb.common.core.constant.ServiceNameConstants;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.datacollect.dto.WeatherDto;
import com.zyw.srsb.datacollect.dto.event.WeatherEvent;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

/**
 * @author 朱耀威
 */
@FeignClient(contextId = "weatherService", value = ServiceNameConstants.DATA_COLLECT_SERVE)
public interface WeatherFeignService {
	/**
	 * 获取天气情况
	 * @param event
	 * @param from
	 * @return
	 */
	@PostMapping("/data-collect/v7/weather/now")
	R<WeatherDto> getNowWeather(@RequestBody WeatherEvent event, @RequestHeader(SecurityConstants.FROM) String from);
}
