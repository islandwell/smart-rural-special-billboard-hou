package com.zyw.srsb.datacollect.dto.reponse;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class ReferDTO {
    @JSONField(name = "sources")
    private List<String> sources;
    @JSONField(name = "license")
    private List<String> license;
}
