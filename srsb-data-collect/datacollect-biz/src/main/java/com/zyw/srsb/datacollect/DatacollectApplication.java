package com.zyw.srsb.datacollect;

import com.zyw.srsb.common.feign.annotation.EnableSrsbFeignClients;
import com.zyw.srsb.common.security.annotation.EnableSrsbResourceServer;
import com.zyw.srsb.common.swagger.annotation.EnableSrsbSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
* @author pig archetype
* <p>
* 项目启动类
*/
@EnableSrsbSwagger2
@EnableSrsbFeignClients
@EnableSrsbResourceServer
@EnableDiscoveryClient
@SpringBootApplication
public class DatacollectApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatacollectApplication.class, args);
    }

}
