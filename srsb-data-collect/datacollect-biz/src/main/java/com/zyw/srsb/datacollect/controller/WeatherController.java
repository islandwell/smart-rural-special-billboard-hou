package com.zyw.srsb.datacollect.controller;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.zyw.srsb.common.core.constant.SecurityConstants;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.security.annotation.Inner;
import com.zyw.srsb.datacollect.dto.event.WeatherEvent;
import com.zyw.srsb.datacollect.dto.reponse.WeatherDto;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/data-collect")
@Api(value = "WeatherController", tags = "天气服务api")
public class WeatherController {

	/**
	 * 获取天气情况
	 *
	 * @return
	 */
	@Inner
	@PostMapping("/v7/weather/now")
	public R<WeatherDto> getNowWeather(@RequestBody WeatherEvent event, @RequestHeader(SecurityConstants.FROM) String from) {
		Map<String, Object> request = new HashMap<>();
		request.put("location", event.getLocation());
		request.put("key", event.getKey());
		try {
			String s = HttpUtil.get(event.getBaseUrl(), request);
			return R.ok(JSONUtil.toBean(s, WeatherDto.class));
		} catch (Exception e) {

		}
		return R.failed();
	}
}
