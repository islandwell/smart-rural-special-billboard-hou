package com.zyw.srsb.datacollect.dto.event;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * 高德地理正向解析响应头
 * @author 朱耀威
 * @date 2022/4/9
 */
@Data
@NoArgsConstructor
@ApiModel(value = "正向解析响应头")
public class GeoEvent {
	@ApiModelProperty(value = "key",required = false)
	private String key;

	@NotBlank(message = "城市不能为空")
	@ApiModelProperty(value = "城市",required = true)
	private String city;

	@NotBlank(message = "地址不能为空")
	@ApiModelProperty(value = "地址",required = true)
	private String address;
}
