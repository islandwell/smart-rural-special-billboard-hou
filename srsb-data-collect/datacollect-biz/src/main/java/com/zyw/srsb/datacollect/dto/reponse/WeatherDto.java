package com.zyw.srsb.datacollect.dto.reponse;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class WeatherDto {

	@JSONField(name = "code")
	private String code;
	@JSONField(name = "updateTime")
	private String updateTime;
	@JSONField(name = "fxLink")
	private String fxLink;
	@JSONField(name = "now")
	private NowDTO now;
	@JSONField(name = "refer")
	private ReferDTO refer;
}
