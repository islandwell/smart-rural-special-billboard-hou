package com.zyw.srsb.datacollect.service;

import com.zyw.srsb.datacollect.dto.event.GeoEvent;
import com.zyw.srsb.datacollect.dto.reponse.GeoDto;

/**
 * 高德api服务
 * @author 朱耀威
 * @date 2022/4/9
 */
public interface GaodeApiService {

	/**
	 * 高德编码解析
	 * @param event
	 * @return
	 */
	GeoDto geoCode(GeoEvent event);
}
