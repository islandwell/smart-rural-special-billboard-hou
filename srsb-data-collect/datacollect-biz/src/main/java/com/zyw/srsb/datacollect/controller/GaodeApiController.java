package com.zyw.srsb.datacollect.controller;

import com.zyw.srsb.common.core.constant.SecurityConstants;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.security.annotation.Inner;
import com.zyw.srsb.datacollect.dto.event.GeoEvent;
import com.zyw.srsb.datacollect.dto.reponse.GeoDto;
import com.zyw.srsb.datacollect.service.GaodeApiService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * 高德api
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/data-collect")
@Api(value = "GaodeApiController", tags = "高德api")
public class GaodeApiController {
	private final GaodeApiService gaodeApiService;

	/**
	 * 高德编码解析
	 *
	 * @return
	 */
	@Inner
	@PostMapping("/v3/geocode/geo")
	public R<GeoDto> geoCode(@RequestBody GeoEvent event, @RequestHeader(SecurityConstants.FROM) String from) {
		return R.ok(gaodeApiService.geoCode(event));
	}
}
