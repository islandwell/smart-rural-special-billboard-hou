package com.zyw.srsb.datacollect.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.zyw.srsb.datacollect.dto.event.GeoEvent;
import com.zyw.srsb.datacollect.dto.reponse.GeoDto;
import com.zyw.srsb.datacollect.service.GaodeApiService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 高德api服务
 *
 * @author 朱耀威
 * @date 2022/4/9
 */
@Service
public class GaodeApiServiceImpl implements GaodeApiService {
	@Value("${gaode-api.geocode.key}")
	private String key;
	@Value("${gaode-api.geocode.url}")
	private String url;

	@Override
	public GeoDto geoCode(GeoEvent event) {
		Map<String, Object> request = new HashMap<>();
		request.put("address", event.getAddress());
		request.put("city", event.getCity());
		request.put("key", key);
		if (StrUtil.isNotBlank(event.getKey())) {
			request.put("key", event.getKey());
		}
		GeoDto geoDto = null;
		try {
			String s = HttpUtil.get(url, request);
			geoDto = JSON.parseObject(s).toJavaObject(GeoDto.class);
		} catch (Exception e) {
		}
		return geoDto;
	}
}
