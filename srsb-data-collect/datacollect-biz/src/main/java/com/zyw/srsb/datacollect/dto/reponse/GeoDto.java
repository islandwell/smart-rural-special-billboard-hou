package com.zyw.srsb.datacollect.dto.reponse;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 高德地图地理编码返回api
 * @author 朱耀威
 */
@NoArgsConstructor
@Data
public class GeoDto {
	private String status;
	private String info;
	private String infocode;
	private String count;
	private List<GeocodesDTO> geocodes;

	@NoArgsConstructor
	@Data
	public static class GeocodesDTO {
		private String formattedAddress;
		private String country;
		private String province;
		private String citycode;
		private String city;
		private String district;
		private List<String> township;
		private NeighborhoodDTO neighborhood;
		private BuildingDTO building;
		private String adcode;
		private String street;
		private String number;
		private String location;
		private String level;

		@NoArgsConstructor
		@Data
		public static class NeighborhoodDTO {
			private List<String> name;
			private List<String> type;
		}

		@NoArgsConstructor
		@Data
		public static class BuildingDTO {
			private List<String> name;
			private List<String> type;
		}
	}
}
