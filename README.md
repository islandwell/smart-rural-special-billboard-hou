<p align="center">
 <img src="https://img.shields.io/badge/srsb-3.4-success.svg" alt="Build Status">
 <img src="https://img.shields.io/badge/Spring%20Cloud-2021-blue.svg" alt="Coverage Status">
 <img src="https://img.shields.io/badge/Spring%20Boot-2.6-blue.svg" alt="Downloads">
 <img src="https://img.shields.io/github/license/srsb-mesh/srsb"/>
</p>


## 系统说明
- 基于 Spring Cloud 2021 、Spring Boot 2.6、 OAuth2 的 RBAC **权限管理系统**
- 基于数据驱动视图的理念封装 element-ui，即使没有 vue 的使用经验也能快速上手
- 提供对常见容器化支持 Docker、Kubernetes、Rancher2 支持
- 提供 lambda 、stream api 、webflux 的生产实践
<br>









