/*
 * Copyright (c) 2020 srsb4cloud Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.zyw.srsb.admin;

import com.zyw.srsb.common.feign.annotation.EnableSrsbFeignClients;
import com.zyw.srsb.common.security.annotation.EnableSrsbResourceServer;
import com.zyw.srsb.common.swagger.annotation.EnableSrsbSwagger2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author 朱耀威
 * @date 2018年06月21日 用户统一管理系统
 */
@EnableSrsbSwagger2
@EnableSrsbResourceServer
@EnableSrsbFeignClients
@EnableDiscoveryClient
@SpringBootApplication
public class SrsbAdminApplication {

	public static void main(String[] args) {
		SpringApplication.run(SrsbAdminApplication.class, args);
	}

}
