/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the srsb4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.zyw.srsb.admin.controller;

import cn.hutool.core.io.FileUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.zyw.srsb.common.core.util.R;
import com.zyw.srsb.common.log.annotation.SysLog;
import com.zyw.srsb.admin.entity.SysImage;
import com.zyw.srsb.admin.service.SysImageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


/**
 * 图片管理
 *
 * @author 朱耀威
 * @date 2021-12-15 00:45:22
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/sysimage" )
@Api(value = "sysimage", tags = "图片管理管理")
public class SysImageController {

    private final  SysImageService sysImageService;

    /**
     * 分页查询
     * @param page 分页对象
     * @param sysImage 图片管理
     * @return
     */
    @ApiOperation(value = "分页查询", notes = "分页查询")
    @GetMapping("/page" )
    @PreAuthorize("@pms.hasPermission('admin_sysimage_get')" )
    public R getSysImagePage(Page page, SysImage sysImage) {
        return R.ok(sysImageService.page(page, Wrappers.query(sysImage)));
    }


    /**
     * 通过id查询图片管理
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id查询", notes = "通过id查询")
    @GetMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('admin_sysimage_get')" )
    public R getById(@PathVariable("id" ) Long id) {
        return R.ok(sysImageService.getById(id));
    }

    /**
     * 新增图片管理
     * @param file 图片对象
	 * @param fileName 文件标识名称
     * @return R
     */
    @ApiOperation(value = "新增图片管理", notes = "新增图片管理")
    @SysLog("新增图片管理" )
    @PostMapping("/{fileName}")
    @PreAuthorize("@pms.hasPermission('admin_sysimage_add')" )
    public R save(@RequestPart("file") MultipartFile file,@PathVariable("fileName") String fileName) {
		// 判断图片格式是否合法
		String type = FileUtil.extName(file.getOriginalFilename()).toUpperCase();
		if(ObjectUtils.isEmpty(file)|| !StringUtils.hasLength(fileName) ||IMG_PRX_STR.indexOf(type) == -1){
			return R.failed("上传得图片格式违法");
		}
		return sysImageService.save(file,fileName);
    }

    /**
     * 修改图片管理
     * @param sysImage 图片管理
     * @return R
     */
    @ApiOperation(value = "修改图片管理", notes = "修改图片管理")
    @SysLog("修改图片管理" )
    @PutMapping
    @PreAuthorize("@pms.hasPermission('admin_sysimage_edit')" )
    public R updateById(@RequestBody SysImage sysImage) {
        return R.ok(sysImageService.updateById(sysImage));
    }

    /**
     * 通过id删除图片管理
     * @param id id
     * @return R
     */
    @ApiOperation(value = "通过id删除图片管理", notes = "通过id删除图片管理")
    @SysLog("通过id删除图片管理" )
    @DeleteMapping("/{id}" )
    @PreAuthorize("@pms.hasPermission('admin_sysimage_del')" )
    public R removeById(@PathVariable Long id) {
        return R.ok(sysImageService.deleteImage(id));
    }

	/**
	 * 上传图片
	 * @param file
	 * @return R
	 */
	@ApiOperation(value = "上传图片", notes = "上传图片")
	@SysLog("上传图片" )
	@PostMapping("/upload/{fileName}" )
	@PreAuthorize("@pms.hasPermission('admin_sysimage_upload')" )
	public R uploadImage(@RequestPart("file") MultipartFile file,@PathVariable("fileName") String fileName) {
		log.info("fileName:{}",fileName);
		return R.ok();
	}

	private static String IMG_PRX_STR="BMP、JPEG、JPG、GIF、PSD、PNG、TIFF、TGA、EPS、SVG、WEBP、CDR、PCX、EXIF、FPX、PCD、DXF、UFO、AI、HDRI、RAW、WMF、FLIC、EMF、ICO";

}
