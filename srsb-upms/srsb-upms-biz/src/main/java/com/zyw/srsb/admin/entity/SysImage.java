/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the srsb4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */
package com.zyw.srsb.admin.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zyw.srsb.common.mybatis.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 图片管理
 *
 * @author 朱耀威
 * @date 2021-12-15 00:45:22
 */
@Data
@TableName("sys_image")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "图片管理")
public class SysImage extends BaseEntity {

    /**
     * 图片id
     */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value="图片id")
    private Long id;

    /**
     * 图片名字
     */
    @ApiModelProperty(value="图片名字")
    private String fileName;

    /**
     * 真实保存的名字
     */
    @ApiModelProperty(value="真实保存的名字")
    private String original;

    /**
     * 图片url
     */
    @ApiModelProperty(value="图片url")
    private String imgUrl;

    /**
     * 缩略图
     */
    @ApiModelProperty(value="缩略图")
    private String thumbUrl;

    /**
     * 图片类型
     */
    @ApiModelProperty(value="图片类型")
    private String type;

    /**
     * 文件大小
     */
    @ApiModelProperty(value="文件大小")
    private Long fileSize;

    /**
     * 逻辑删除状态
     */
    @ApiModelProperty(value="逻辑删除状态")
    private String delFlag;


}
