package com.zyw.srsb.admin.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 阿里oss配置
 */
@Data
@Component
@ConfigurationProperties(
		prefix = "aliyun"
)
public class AliOssConfig {
	private String endpoint;
	private String accessKeyId;
	private String accessKeySecret;
	private String baseDir;
	private String bucket;
	private String baseUrl;
	private String imagePath;
}
