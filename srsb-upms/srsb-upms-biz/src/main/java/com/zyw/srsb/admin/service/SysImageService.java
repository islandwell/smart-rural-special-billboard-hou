/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the srsb4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.zyw.srsb.admin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zyw.srsb.admin.entity.SysImage;
import com.zyw.srsb.common.core.util.R;
import org.springframework.web.multipart.MultipartFile;

/**
 * 图片管理
 *
 * @author 朱耀威
 * @date 2021-12-15 00:45:22
 */
public interface SysImageService extends IService<SysImage> {
	/**
	 * 新增图片
	 * @param file
	 * @param fileName
	 * @return
	 */
	R save(MultipartFile file, String fileName);

	/**
	 * 根据id删除指定图片
	 * @param id
	 * @return
	 */
	Boolean deleteImage(Long id);
}
